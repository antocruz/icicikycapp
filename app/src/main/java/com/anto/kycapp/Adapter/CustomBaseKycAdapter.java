package com.anto.kycapp.Adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anto.kycapp.Fragments.UploadKycFragment;
import com.anto.kycapp.Models.PendingkycResponse;
import com.anto.kycapp.Models.PendingsignupResponse;
import com.anto.kycapp.R;
import com.anto.kycapp.Utils.Constants;
import com.anto.kycapp.Utils.FontChangeCrawler;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.util.ArrayList;

/**
 * Created by anto on 2/14/2018.
 */

public class CustomBaseKycAdapter extends BaseAdapter implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    public static final int PERMISSION_ID = 101;
    public static final int LOCATIONACT_ID = 151;
    ArrayList<PendingkycResponse> myList = new ArrayList();
    LayoutInflater inflater;
    FragmentActivity context;
    public   int cellposition;
    public static GoogleApiClient googleApiClient;
    public static LocationRequest locationRequest;

    public CustomBaseKycAdapter(){

    }
    public CustomBaseKycAdapter(FragmentActivity context, ArrayList<PendingkycResponse> myList) {
        this.myList = myList;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public PendingkycResponse getItem(int position) {
        return myList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;
        cellposition = position;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_list_item, parent, false);

            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        FontChangeCrawler fontChanger = new FontChangeCrawler(context.getAssets(), context.getResources().getString(R.string.appfontBold));
        fontChanger.replaceFonts((ViewGroup)convertView.findViewById(R.id.listitemlay));


        mViewHolder.corporate_txt.setText(myList.get(position).getBusiness());
        mViewHolder.customerid_txt.setText(myList.get(position).getEntityId());
        mViewHolder.dateandtime_txt.setText(myList.get(position).getCreated());
        if (myList.get(position).getKycStatus().equals("UPLOAD_KYC")) {
            mViewHolder.statuslay.setVisibility(View.GONE);
            mViewHolder.textView2.setVisibility(View.VISIBLE);
        } else {
            mViewHolder.statuslay.setVisibility(View.VISIBLE);
            mViewHolder.textView2.setVisibility(View.GONE);
            mViewHolder.status_txt.setText(myList.get(position).getKycStatus());
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermissions()) {
                    if (isLocationEnabled()) {
                        if (myList.get(position).getKycStatus().equals("FAILED") || myList.get(position).getKycStatus().equals("PENDING")) {
                            context.getSupportFragmentManager().beginTransaction()
                                    .addToBackStack("mainactivity")
                                    .add(R.id.pendingsignuplay, new UploadKycFragment(myList.get(position).getKycStatus(), myList.get(position).getEntityId(), myList.get(position).getId(), myList.get(position).getBusiness(), myList.get(position).geteKycRefNo(), myList.get(position).getKycReason())).commit();
                        }
                    }
                    else {
                       displayLocationSettingsRequest(context);
                    }
                }
                    else {
                        requestPermissions();
                }
            }
        });

        //Toast.makeText(context, myList.get(position).getStatus(), Toast.LENGTH_SHORT).show();
        return convertView;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private class MyViewHolder {
        TextView corporate_txt, customerid_txt,dateandtime_txt,status_txt,textView2;
        RelativeLayout  content_baselay;
        ImageView img_trans;
        LinearLayout statuslay;

        public MyViewHolder(View item) {
            corporate_txt = item.findViewById(R.id.corporate_txt);
            customerid_txt = item.findViewById(R.id.customerid_txt);
            dateandtime_txt = item.findViewById(R.id.dateandtime_txt);
            status_txt = item.findViewById(R.id.status_txt);
            statuslay = item.findViewById(R.id.statuslay);
            textView2 = item.findViewById(R.id.textView2);
        }
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                context,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }


    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }



    public  void displayLocationSettingsRequest(final Activity context) {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            googleApiClient.connect();
        }

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000);        // 10 seconds, in milliseconds
        locationRequest.setFastestInterval(1 * 1000); // 1 second, in milliseconds

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                // Toast.makeText(GooglePlacesActivity.this, status.getStatusMessage(), Toast.LENGTH_SHORT).show();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i("", "All location settings are satisfied.");

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i("", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(context, LOCATIONACT_ID);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i("", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i("", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

}
