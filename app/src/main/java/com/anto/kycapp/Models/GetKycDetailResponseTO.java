package com.anto.kycapp.Models;

import java.util.List;

/**
 * Created by anto on 5/21/2019.
 */

public class GetKycDetailResponseTO {
     Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        private List<Document> documents = null;

        public List<Document> getDocuments() {
            return documents;
        }

        public void setDocuments(List<Document> documents) {
            this.documents = documents;
        }
    }
}
