package com.anto.kycapp.Models;

public class ValidationRequestTo {
    private String idType,idNumber,entityId,idTypeValidation,otpReferenceNo,otp;

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getIdTypeValidation() {
        return idTypeValidation;
    }

    public void setIdTypeValidation(String idTypeValidation) {
        this.idTypeValidation = idTypeValidation;
    }

    public String getOtpReferenceNo() {
        return otpReferenceNo;
    }

    public void setOtpReferenceNo(String otpReferenceNo) {
        this.otpReferenceNo = otpReferenceNo;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
