package com.anto.kycapp.Models;

/**
 * Created by anto on 3/14/2019.
 */

public class CreateorEditKycRequestTo {

    private String customerId;
    private String business;
    private String ekycRefNo,bank;
    private int documents;

    private String agentName,agentId,location,lattitude,longitude;


    public String getBankName() {
        return bank;
    }

    public void setBankName(String bankName) {
        this.bank = bankName;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLattitude() {
        return lattitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    private String id;
    private String authLevel;
    private String userId;

    public String getEkycRefNo() {
        return ekycRefNo;
    }

    public void setEkycRefNo(String ekycRefNo) {
        this.ekycRefNo = ekycRefNo;
    }

    public int getDocuments() {
        return documents;
    }

    public void setDocuments(int documents) {
        this.documents = documents;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthLevel() {
        return authLevel;
    }

    public void setAuthLevel(String authLevel) {
        this.authLevel = authLevel;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
