package com.anto.kycapp.Models;

/**
 * Created by anto on 5/21/2019.
 */

public class GetKycRequestTo {
    String business,kycRefNo,bank;

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getKycRefNo() {
        return kycRefNo;
    }

    public void setKycRefNo(String kycRefNo) {
        this.kycRefNo = kycRefNo;
    }
}
