package com.anto.kycapp.Models;

import java.util.ArrayList;
import java.util.List;

public class AddkycAgentResponseTO extends ResponseTO{
    public Body body;

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public class Body {
        List<Result> result = new ArrayList<>();

        public List<Result> getResults() {
            return result;
        }

        public void setResults(List<Result> results) {
            this.result = results;
        }

    }

    public class Result {
        String id,business,created,entityId,maker,kycStatus,kycReason,eKycRefNo,kitNo,ekycRefNo;


        public String getEkycRefNo() {
            return ekycRefNo;
        }

        public void setEkycRefNo(String ekycRefNo) {
            this.ekycRefNo = ekycRefNo;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBusiness() {
            return business;
        }

        public void setBusiness(String business) {
            this.business = business;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getEntityId() {
            return entityId;
        }

        public void setEntityId(String entityId) {
            this.entityId = entityId;
        }

        public String getMaker() {
            return maker;
        }

        public void setMaker(String maker) {
            this.maker = maker;
        }

        public String getKycStatus() {
            return kycStatus;
        }

        public void setKycStatus(String kycStatus) {
            this.kycStatus = kycStatus;
        }

        public String getKycReason() {
            return kycReason;
        }

        public void setKycReason(String kycReason) {
            this.kycReason = kycReason;
        }

        public String geteKycRefNo() {
            return eKycRefNo;
        }

        public void seteKycRefNo(String eKycRefNo) {
            this.eKycRefNo = eKycRefNo;
        }

        public String getKitNo() {
            return kitNo;
        }

        public void setKitNo(String kitNo) {
            this.kitNo = kitNo;
        }
    }
}
