package com.anto.kycapp.Models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by anto on 2/5/2019.
 */

public class MenuData {

    private String menuName;
    private String menuLink;
    private String menuIcon;
    private List<Object> subMenuList = null;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuLink() {
        return menuLink;
    }

    public void setMenuLink(String menuLink) {
        this.menuLink = menuLink;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    public List<Object> getSubMenuList() {
        return subMenuList;
    }

    public void setSubMenuList(List<Object> subMenuList) {
        this.subMenuList = subMenuList;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
