package com.anto.kycapp.Models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anto on 2/18/2019.
 */

public class PendingkycResponseTo {


    List<Result> result = new ArrayList<>();

    public List<Result> getResults() {
        return result;
    }

    public void setResults(List<Result> results) {
        this.result = results;
    }

    public static class Result{

        String id,business,created,entityId,maker,kycStatus,kycReason,eKycRefNo,kitNo;

        public String getKitNo() {
            return kitNo;
        }

        public void setKitNo(String kitNo) {
            this.kitNo = kitNo;
        }

        public String getKycReason() {
            return kycReason;
        }

        public void setKycReason(String kycReason) {
            this.kycReason = kycReason;
        }

        public String geteKycRefNo() {
            return eKycRefNo;
        }

        public void seteKycRefNo(String eKycRefNo) {
            this.eKycRefNo = eKycRefNo;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBusiness() {
            return business;
        }

        public void setBusiness(String business) {
            this.business = business;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getKycStatus() {
            return kycStatus;
        }

        public void setKycStatus(String kycStatus) {
            this.kycStatus = kycStatus;
        }

        public String getEntityId() {
            return entityId;
        }

        public void setEntityId(String entityId) {
            this.entityId = entityId;
        }

        public String getMaker() {
            return maker;
        }

        public void setMaker(String maker) {
            this.maker = maker;
        }
    }

}
