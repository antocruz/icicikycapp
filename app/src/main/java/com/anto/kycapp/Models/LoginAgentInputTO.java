package com.anto.kycapp.Models;

public class LoginAgentInputTO {
    String userName, password,authType;

    public String getUsername() {
        return userName;
    }

    public void setUsername(String username) {
        this.userName = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthtype() {
        return authType;
    }

    public void setAuthtype(String authtype) {
        this.authType = authtype;
    }
}
