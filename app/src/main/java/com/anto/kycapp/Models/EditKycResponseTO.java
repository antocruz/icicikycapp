package com.anto.kycapp.Models;

/**
 * Created by anto on 5/22/2019.
 */

public class EditKycResponseTO {
    String result;
    String exception;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }
}
