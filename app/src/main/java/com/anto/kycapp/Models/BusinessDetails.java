package com.anto.kycapp.Models;

import java.util.List;

/**
 * Created by anto on 2/5/2019.
 */

public class BusinessDetails {
    String name,noOfAuthLevel,description,bin,bankName,bankDescription;
    List<CorporateDetails> corporateDetails;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNoOfAuthLevel() {
        return noOfAuthLevel;
    }

    public void setNoOfAuthLevel(String noOfAuthLevel) {
        this.noOfAuthLevel = noOfAuthLevel;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public List<CorporateDetails> getCorporateDetails() {
        return corporateDetails;
    }

    public void setCorporateDetails(List<CorporateDetails> corporateDetails) {
        this.corporateDetails = corporateDetails;
    }
}
