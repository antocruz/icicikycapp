package com.anto.kycapp.Utils;

import android.content.Context;

import java.io.File;

/**
 * Created by anto on 1/19/2019.
 */

public class DeviceUtils {

    public Boolean isDeviceRooted(Context context){
       // boolean isRooted = isrooted1() || isrooted2();
        return isrooted2();
    }

    private boolean isrooted1() {

        File file = new File("/system/app/Superuser.apk");
        return file.exists();
    }

    // try executing commands
    private boolean isrooted2() {
        boolean flag;
        try {
            Process p = Runtime.getRuntime().exec("su");
            p.destroy();
            flag = true;
        } catch (Exception e) {
            flag = false;
        }
        return flag;
    }



    private static boolean canExecuteCommand(String command) {
        boolean executedSuccesfully;
        try {
            Runtime.getRuntime().exec(command);
            executedSuccesfully = true;
        } catch (Exception e) {
            executedSuccesfully = false;
        }

        return executedSuccesfully;
    }


}
