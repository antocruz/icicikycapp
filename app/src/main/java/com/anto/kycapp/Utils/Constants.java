package com.anto.kycapp.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.anto.kycapp.BuildConfig;
import com.anto.kycapp.Interface.EkycServiceApi;
import com.anto.kycapp.MainActivity;
import com.anto.kycapp.Models.Exception;
import com.anto.kycapp.Models.ResponseTO;
import com.anto.kycapp.R;
import com.google.gson.Gson;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedByteArray;

/**
 * Created by anto on 9/29/2018.
 */

public class Constants {
    //public static final String securityToken = "EDFGHIUERHTDFHGIEURHTDFGNKHGEIRUUWTHHFFEDERALPREPROD";
    //public static final String securityToken = "QEDFGHIUERHTDFHGIEURHTDFGNKHGEIRUUWTHHM2P";
    public static final String securityToken = "QEDFGHIUERHTDFHGIEURHTDFGNKHGEIRPREPAIDFEDERALPROD1";
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static String BASE_URL = BuildConfig.BaseUrl;
    public static String APP_PREFS = "KYCPREFS";
    public static String SESSION_PREFS = "sessionpref";
    public static String REM_PREFS = "rememberpref";
    public static String MENU_KEY = "MENUKEY";
    //public static String TENANT = "M2PDEMO";
    public static String TENANT = "GOPROCESSING";
    public static String isEkyc;
    public static final long TIMEOUT = 30;
    static RequestInterceptor requestInterceptor;
    static RestAdapter serviceAdapter;
    public static Dialog dialog,uploadeddialog;
    private static SharedPreferences sharedPref;
    static HashMap<String, String> vehicletaghash = new HashMap<>();
    public static class serviceUrls{
        public static final String MANAGER_REGISTRATION = "/registration-manager/";
        public static final String LOGIN_KYC = "/app/validateLogin";
        public static final String VALIDATE_USER = "/app/validateUser";
        public static final String OTP_AGENT = "/auth/user";
        public static final String FETCH_PENDINGSIGNUP = "/app/getRegistrationData";
        public static final String FETCH_PENDINGKYC = "/app/getKycListData";
        public static final String ADDUSER = "/app/registration";
        public static final String UPDATEUSER = "/app/editRegistration ";
        public static final String RESENDOTP_AGENT = "/auth/resendotp";
        public static final String ADDKYC_AGENT = "/app/editKycRefNo";
        public static final String CHECK_USER = "/app/checkCustomerId/{customerid}";
        public static final String VALIDATECARDANDKIT = "/app/validateKitAndCard";
        public static final String CAPTUREONLYKYC = "/app/captureOnlyKyc";
        public static final String CREATEKYC = "/app/uploadKyc/create";
        public static final String EDITKYC = "/app/uploadKycViaApiv2";
        public static final String FETCHBYKIT = "/app/fetchEntityByCriteria";
        public static final String GETUSERDETAILS = "/app/viewRegistration/{id}";
        public static final String GETKYCDETAILS = "/app/fetchKycViaRefNo/doc";
        public static final String FOREX_REGISTER = MANAGER_REGISTRATION + "register";
        public static String SESSIONTOKEN;

        //panvalidation
        public static final String BASEURL = "https://validationuat.yappay.in/";
        public static final String VALIDATION = "/app/validate/";
        public static final String PANVALIDATION = VALIDATION+"pan";
        public static final String GENERATEOTP = VALIDATION+"generateOtp";
        public static final String VALIDATEOTP = VALIDATION+"validateOtp";
    }

    public static List<String> getSharedPreferenceStringList(Context pContext, String pKey) {
        int size = pContext.getSharedPreferences(Constants.APP_PREFS, Activity.MODE_PRIVATE).getInt(pKey + "size", 0);
        List<String> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add(pContext.getSharedPreferences(Constants.APP_PREFS, Activity.MODE_PRIVATE).getString(pKey + i, ""));
        }
        return list;
    }

    public static void setSharedPreferenceStringList(Context pContext, String pKey, List<String> pData) {
        SharedPreferences.Editor editor = pContext.getSharedPreferences(Constants.APP_PREFS, Activity.MODE_PRIVATE).edit();
        editor.putInt(pKey + "size", pData.size());
        editor.commit();

        for (int i = 0; i < pData.size(); i++) {
            SharedPreferences.Editor editor1 = pContext.getSharedPreferences(Constants.APP_PREFS, Activity.MODE_PRIVATE).edit();
            editor1.putString(pKey + i, (pData.get(i)));
            editor1.commit();
        }
    }

    public static void setvehiclehashmap(){
        vehicletaghash.put("CAR/JEEP/VAN,2 AXLE","VC4,VIOLET");
        vehicletaghash.put("LCV,2 AXLE","VC5,ORANGE");
        vehicletaghash.put("BUS,3 AXLE","VC6,YELLOW");
        vehicletaghash.put("TRUCK,3 AXLE","VC6,YELLOW");
        vehicletaghash.put("BUS/MINI BUS,2 AXLE","VC7,GREEN");
        vehicletaghash.put("TRUCK,2 AXLE","VC7,GREEN");
        vehicletaghash.put("TRUCK,4 to 6 AXLE","VC12,PINK");
        vehicletaghash.put("TRUCK,7 AXLE n Above","VC15,BLUE");
        vehicletaghash.put("EARTH MOVERS/HCM,EARTH MOVERS/HCM","VC16,BLACK");
    }

    public static String getTag(String key){
        if (vehicletaghash.get(key)!=null) {
            return vehicletaghash.get(key);
        } else {
            return "";
        }
    }

    public static void deleteSharedPreferences(Context pContext){
        SharedPreferences.Editor editor = pContext.getSharedPreferences(Constants.APP_PREFS, Activity.MODE_PRIVATE).edit();
        editor.clear().commit();
    }

    public static void deleteSessionPreferences(Context pContext){
        SharedPreferences.Editor editor = pContext.getSharedPreferences(REM_PREFS, Context.MODE_PRIVATE).edit();
        editor.clear().commit();
    }

    public static void toastMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static RestAdapter getUserServiceAdapter(Activity activity) {
        OkHttpClient okHttp = new OkHttpClient();
        okHttp.setConnectTimeout(Constants.TIMEOUT, TimeUnit.SECONDS);
        okHttp.setRetryOnConnectionFailure(true);

        serviceAdapter = new RestAdapter.Builder()
                .setEndpoint(BuildConfig.KycBaseUrl)
                .setRequestInterceptor(getUserRequestInterceptor(activity))
                .setLogLevel(getLoglevel())
                .setClient(new OkClient(okHttp))
                .build();

        return serviceAdapter;
    }


    public static RestAdapter.LogLevel getLoglevel(){
        if(BuildConfig.DEBUG){
            return RestAdapter.LogLevel.FULL;
        }
        else{
            return RestAdapter.LogLevel.NONE;
        }
    }



    public static RestAdapter getServiceAdapter(Activity activity, String isEkyc) {
        //System.out.println("THe TENANT inside hhtpclien"+isEkyc);
        OkHttpClient okHttp = new OkHttpClient();
        okHttp.setConnectTimeout(Constants.TIMEOUT, TimeUnit.SECONDS);
        okHttp.setRetryOnConnectionFailure(true);
            if(isEkyc.equals("yes")) {
                serviceAdapter = new RestAdapter.Builder()
                        .setEndpoint(BuildConfig.KycBaseUrl)
                        .setRequestInterceptor(getRequestInterceptor(activity, isEkyc))
                        .setLogLevel(getLoglevel())
                        .setClient(new OkClient(okHttp))
                        .build();
            }
            else if(isEkyc.equals("adduser")){
                if (TENANT.equals("FXBPAY")) {
                    serviceAdapter = new RestAdapter.Builder()
                            .setEndpoint(BuildConfig.KycBaseUrl)
                            .setRequestInterceptor(getRequestInterceptor(activity, isEkyc))
                            .setLogLevel(getLoglevel())
                            .setClient(new OkClient(okHttp))
                            .build();
                } else {
                    serviceAdapter = new RestAdapter.Builder()
                            .setEndpoint(BuildConfig.KycBaseUrl)
                            .setRequestInterceptor(getRequestInterceptor(activity, isEkyc))
                            .setLogLevel(getLoglevel())
                            .setClient(new OkClient(okHttp))
                            .build();
                }
            }
            else{
                serviceAdapter = new RestAdapter.Builder()
                        .setEndpoint(BuildConfig.KycBaseUrl)
                        .setRequestInterceptor(getRequestInterceptor(activity, isEkyc))
                        .setLogLevel(getLoglevel())
                        .setClient(new OkClient(okHttp))
                        .build();
            }
        return serviceAdapter;
    }


    public static EkycServiceApi getEkycServicesAPI(Activity activity, String isEkyc) {
        //System.out.println("THe TENANT"+isEkyc);
        return getServiceAdapter(activity,isEkyc).create(EkycServiceApi.class);
    }

    public static EkycServiceApi getUsersAPI(Activity activity) {
        return getUserServiceAdapter(activity).create(EkycServiceApi.class);
    }

    public static EkycServiceApi getValidateApi(Activity activity) {
        TENANT = "INDWEALTH";
        return getServiceAdapter(activity,"adduser").create(EkycServiceApi.class);
    }

    private static RequestInterceptor getRequestInterceptor(final Activity activity,  String params) {
        isEkyc = params;
        return requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
               // System.out.println("requestInterceptor ++++++++"+isEkyc);
                if(isEkyc.equals("yes")){
                    request.addHeader("Content-Type","application/json");
                }
                else if(isEkyc.equals("adduser")){
                    String token = "Basic "+ serviceUrls.SESSIONTOKEN;
                    request.addHeader("Authorization",token);
                    request.addHeader("Content-Type","application/json");
                    request.addHeader("TENANT",TENANT);
                }
                else if(isEkyc.equals("getusers")){
                    String token = "Basic "+ serviceUrls.SESSIONTOKEN;
                    //System.out.println("requestInterceptor"+token);
                    request.addHeader("Authorization",token);
                    request.addHeader("Content-Type","application/json");
                }
                else{
                    request.addHeader("Content-Type","application/json");
                    request.addHeader("TENANT",isEkyc);
                    String token = "Basic "+ serviceUrls.SESSIONTOKEN;
                    //System.out.println("requestInterceptor"+token);
                    request.addHeader("Authorization",token);
                }
            }
        };
    }


    private static RequestInterceptor getUserRequestInterceptor(final Activity activity) {
        return requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {

                String token = "Basic "+ serviceUrls.SESSIONTOKEN;
                //System.out.println("requestInterceptor"+token);
                request.addHeader("Authorization",token);
                request.addHeader("Content-Type","application/json");
            }
        };
    }

    public static boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);

        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable)drawable).getBitmap() != null;
        }

        return hasImage;
    }


    public static boolean checkIfException(Context context, ResponseTO encryptedResponseTO) {
        if (encryptedResponseTO == null) {
            return true;
        } else {
            Exception exception = encryptedResponseTO.getException();
            if (exception != null) {
                if ( (!exception.getDetailMessage().equals(""))) {
                    Toast.makeText(context,  exception.getDetailMessage(), Toast.LENGTH_SHORT).show();
                    System.out.println("The error is"+exception);
                }  else {
                    Toast.makeText(context, "Something went wrong! Please try later", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
            return false;
        }
    }

    public static void ShowErrorHandlerMessage(Context context, RetrofitError error) {
        try {
            String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
            ResponseTO responseTo = new Gson().fromJson(json, ResponseTO.class);
            String exception = responseTo.getException().getDetailMessage();
            if (exception != null) {
                if ((!exception.equals(""))) {
                    Toast.makeText(context,  exception, Toast.LENGTH_SHORT).show();
                    System.out.println("The error is"+exception);
                }  else {
                    Toast.makeText(context, "Something went wrong! Please try later", Toast.LENGTH_SHORT).show();
                }

            }
        } catch (java.lang.Exception ex) {
            ResponseErrorValidator.ShowMessage(context, error);
            //Utils.toastMessage(context, context.getString(R.string.connection_failed));
        }
    }

    public static Uri overwriteBitmap(Context context, String fileName, Bitmap bm, Bitmap.CompressFormat format, int quality, String childfilename) {

        File imageFile = new File(new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), childfilename), fileName);
        if (imageFile.exists()) {
            imageFile.delete();
        }
        else {
            imageFile.getParentFile().mkdirs();
        }

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imageFile);
            bm.compress(format,quality,fos);
            fos.close();
        }
        catch (IOException e) {
            Log.e("app",e.getMessage());
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return FileProvider.getUriForFile(context, "com.anto.kycapp.provider", imageFile);
    }

    public static  int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    private static String getRealPathFromURI(Context activity,String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = activity.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    @SuppressLint("SimpleDateFormat")
    public static  String getDateCurrentTimeZone(long timestamp) {
        try{
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(timestamp);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
            Date currenTimeZone = calendar.getTime();
            return sdf.format(currenTimeZone);
        }catch (java.lang.Exception e) {
        }
        return "";
    }


    public static void showLoading(Activity activity){
        dialog = new Dialog(activity,android.R.style.Theme_Material_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.loading_view);
        dialog.show();
    }


    public static void showCustomDialog(final Activity activity,final String message){
        dialog = new Dialog(activity,android.R.style.Theme_Material_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.mobilenumber_view);
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), activity.getResources().getString(R.string.appfont));
        fontChanger.replaceFonts((ViewGroup)dialog.findViewById(R.id.kitnolay));

        final TextView msg_txt;
        final TextView submituser_txt;



        msg_txt= (TextView)dialog.findViewById(R.id.et_kit);

        if (message!=null) {
            msg_txt.setText(message);
        }

        submituser_txt= (TextView) dialog.findViewById(R.id.submituser_txt);


        submituser_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void showUploadeddialog(final Activity activity){
        uploadeddialog = new Dialog(activity,android.R.style.Theme_Material_Dialog_Alert);
        uploadeddialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        uploadeddialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        uploadeddialog.setCancelable(false);
        uploadeddialog.setContentView(R.layout.kycuploaded_view);
        TextView ok = uploadeddialog.findViewById(R.id.textOk);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadeddialog.dismiss();
                Intent intent = new Intent(activity,MainActivity.class);
                activity.startActivity(intent);
                activity.finish();
            }
        });
        uploadeddialog.show();
    }




    public static void dismissloading(){
        dialog.dismiss();
    }

    public static void setSharedPref(Activity context, String prefKey, String editorKey, String tokenString) {
        sharedPref = context.getSharedPreferences(prefKey, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(editorKey, tokenString);
        editor.apply();
    }

    public static String getSharedPref(Activity context, String prefKey, String editorKey) {
        sharedPref = context.getSharedPreferences(prefKey, Context.MODE_PRIVATE);
        return sharedPref.getString(editorKey, "");
    }

    public static boolean  isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        if (netInfo != null) {
            return (netInfo != null && netInfo.isConnectedOrConnecting());
        } else {
            Integer linkSpeed = wifiInfo.getLinkSpeed(); //measured using WifiInfo.LINK_SPEED_UNITS
            if (linkSpeed > 0) {
                return (netInfo != null && netInfo.isConnected());
            } else {
                return false;
            }
        }
    }
}
