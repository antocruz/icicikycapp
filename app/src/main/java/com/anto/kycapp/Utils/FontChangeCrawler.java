package com.anto.kycapp.Utils;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by anto on 11/14/2017.
 */

public class FontChangeCrawler
{
    private Typeface typeface;

    public FontChangeCrawler(Typeface typeface)
    {
        this.typeface = typeface;
    }

    public FontChangeCrawler(AssetManager assets, String assetsFontFileName)
    {
        typeface = Typeface.createFromAsset(assets, assetsFontFileName);
    }

    public void replaceFontsTextView(TextView textView){
        textView.setTypeface(typeface);
    }

    public void replaceFonts(ViewGroup viewTree)
    {
        try {
            View child;
            for(int i = 0; i < viewTree.getChildCount(); ++i)
            {
                child = viewTree.getChildAt(i);
                if(child instanceof ViewGroup)
                {
                    // recursive call
                    replaceFonts((ViewGroup)child);
                }
                else if(child instanceof TextView)
                {
                    // base case
                    ((TextView) child).setTypeface(typeface);
                }
                else if(child instanceof Button)
                {
                    // base case
                    ((Button) child).setTypeface(typeface);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
