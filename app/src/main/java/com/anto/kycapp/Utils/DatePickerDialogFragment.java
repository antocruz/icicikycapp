package com.anto.kycapp.Utils;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.TextView;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DatePickerDialogFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    TextView btnView;
    Calendar calendar;
    String from;

    public DatePickerDialogFragment() {

    }

    @SuppressLint("ValidFragment")
    public DatePickerDialogFragment(TextView _btnView) {
        btnView = _btnView;
    }

    @SuppressLint("ValidFragment")
    public DatePickerDialogFragment(TextView _btnView, Calendar _c,String type) {
        btnView = _btnView;
        calendar = _c;
        from = type;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int year;
        Calendar c = Calendar.getInstance();
        if (calendar != null) {
            c = calendar;
            year = c.get(Calendar.YEAR);
        } else {
            year = c.get(Calendar.YEAR) - 18;
        }
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar userAge = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        Calendar minAdultAge = new GregorianCalendar();
        minAdultAge.add(Calendar.YEAR, -18);
        if (from.equals("dob")) {
            if (minAdultAge.before(userAge)) {
                Constants.toastMessage(getActivity(), "Sorry! The minimum age for to register is 18 years");
                return;
            }
        }
        Calendar c = Calendar.getInstance();
        c.set(year, monthOfYear, dayOfMonth);
        btnView.setText(Constants.dateFormat.format(c.getTime()));
    }

}