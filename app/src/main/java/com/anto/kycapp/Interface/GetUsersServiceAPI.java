package com.anto.kycapp.Interface;

import com.anto.kycapp.Models.AgentLoginResponseTO;
import com.anto.kycapp.Models.CaptureKycDatainputTo;
import com.anto.kycapp.Models.EditKycResponseTO;
import com.anto.kycapp.Models.PendingSignupResponseTo;
import com.anto.kycapp.Models.UserdataInputTo;
import com.anto.kycapp.Utils.Constants;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by anto on 2/19/2019.
 */

public interface GetUsersServiceAPI {

    //Adduser
    @Multipart
    @POST(Constants.serviceUrls.ADDUSER)
    Call<AgentLoginResponseTO> adduser(@PartMap() Map<String, RequestBody> partMap,@Part List<MultipartBody.Part> files);

     //Edituser
     @Multipart
     @POST(Constants.serviceUrls.UPDATEUSER)
     Call<AgentLoginResponseTO> updateUser(@PartMap() Map<String, RequestBody> partMap,@Part List<MultipartBody.Part> files);

    //capturekyc
    @Multipart
    @POST(Constants.serviceUrls.CAPTUREONLYKYC)
    Call<AgentLoginResponseTO> capturekyc(@PartMap() Map<String, RequestBody> partMap, @Part List<MultipartBody.Part> files);

    //createKyc
    @Multipart
    @POST(Constants.serviceUrls.CREATEKYC)
    Call<AgentLoginResponseTO> createKyc(@PartMap() Map<String, RequestBody> partMap, @Part List<MultipartBody.Part> files);

    //editKyc
    @Multipart
    @POST(Constants.serviceUrls.EDITKYC)
    Call<EditKycResponseTO> editKyc(@PartMap() Map<String, RequestBody> partMap, @Part List<MultipartBody.Part> files);





}
