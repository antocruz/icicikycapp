package com.anto.kycapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.CardView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anto.kycapp.Adapter.CustomBaseKycAdapter;
import com.anto.kycapp.Fragments.CapturekycFragment;
import com.anto.kycapp.Fragments.PendingKycFragment;
import com.anto.kycapp.Fragments.PendingSignUpFragment;
import com.anto.kycapp.Fragments.VehicleSignUpFragment;
import com.anto.kycapp.Interface.GetUsersServiceAPI;
import com.anto.kycapp.Models.AdduserrequestTO;
import com.anto.kycapp.Models.AgentLoginResponseTO;
import com.anto.kycapp.Models.ResponseTO;
import com.anto.kycapp.Models.UserdataInputTo;
import com.anto.kycapp.Utils.Constants;
import com.anto.kycapp.Utils.CustomTypefaceSpan;
import com.anto.kycapp.Utils.DatePickerDialogFragment;
import com.anto.kycapp.Utils.FileUtils;
import com.anto.kycapp.Utils.FontChangeCrawler;
import com.anto.kycapp.Utils.IoUtils;
import com.google.gson.Gson;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.williamww.silkysignature.views.SignaturePad;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static final String APP_TAG = "MyKycApp";
    ImageView multiuser_img,imageView2,imageView3,imageView4,imageView5,imageView6;
    UserdataInputTo userdataInputTo;
    List<MultipartBody.Part> parts = new ArrayList<>();
    int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    public static final int EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 2;
    public static final int EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_READ = 3;
    protected static final int REQUEST_CAMERA = 4;
    protected static final int PICK_IMAGE_REQUEST = 1;
    public String photoFileName = "kycdocument",proofphoto="",imageFileName,imageFilePath;
    public Uri picUri,picUri1,picUri2,picUri3,picUri4;
    CardView customerreg1,customerreg2,customerreg3,customerreg4,gotoekyc;
    TextView next_txt1,next_txt2,next_txt3,text_proof1,text_proof2,text_proof3,text_proof4,text_proof5;
    TextView signup1,signup2,signup3,signup4,signup5,clear;
    SignaturePad signaturePad;
    Toolbar toolbar;
    LinearLayout prooflinearlay1,prooflinearlay2,prooflinearlay3,prooflinearlay4,prooflinearlay5;
    LinearLayout signupprooflay1,signupprooflay2,signupprooflay3,signupprooflay4,signupprooflay5;
    public AdduserrequestTO adduserrequestTO = new AdduserrequestTO();
    MaterialEditText et_firstname,et_lastname,et_mobileNumber,et_customerId,et_kitnumber,et_mailId,et_addressline,et_addressline1,et_city,et_pincode,et_idnumber;
    TextView txt_dateofbirth,idexpiry,back_txt1,back_txt2,back_txt3;
    Spinner titleSpinner,genderSpinner,cardtypeSpinner,corporateSpinner,stateSpinner,proofSpinner,kyctypeSpinner,proofSpinner1,proofSpinner2,proofSpinner3,proofSpinner4,proofSpinner5;
    boolean usercheckonce = false;
    int[] myImageList = new int[]{R.drawable.pending_img/* R.drawable.vehicle_img*/,R.drawable.support,R.drawable.about,R.drawable.logout_ic};

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FontChangeCrawler fontChanger = new FontChangeCrawler(this.getAssets(), this.getResources().getString(R.string.appfont));
        fontChanger.replaceFonts((ViewGroup)findViewById(R.id.mainlayout));
        toolbar = (Toolbar)findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        Gson gson = new Gson();
        String json = Constants.getSharedPref(this,"usermodelkey","usermodelclass");
        userdataInputTo = gson.fromJson(json, UserdataInputTo.class);

        Constants.serviceUrls.SESSIONTOKEN = Constants.getSharedPref(this,"sessiontoken","securitytoken");
        //System.out.println("THe token is"+Constants.serviceUrls.SESSIONTOKEN);


        multiuser_img = (ImageView)findViewById(R.id.multiuser_img);
        imageView2 = (ImageView)findViewById(R.id.imageView2);
        imageView3 = (ImageView)findViewById(R.id.imageView3);
        imageView4 = (ImageView)findViewById(R.id.imageView4);
        imageView5 = (ImageView)findViewById(R.id.imageView5);
        imageView6 = (ImageView)findViewById(R.id.imageView6);

        et_firstname = (MaterialEditText)findViewById(R.id.firstname);
        et_lastname = (MaterialEditText)findViewById(R.id.lastname);
        et_mobileNumber = (MaterialEditText)findViewById(R.id.mobileNumber);
        et_customerId = (MaterialEditText)findViewById(R.id.customerId);
        et_kitnumber = (MaterialEditText)findViewById(R.id.kitNumber);
        et_mailId = (MaterialEditText)findViewById(R.id.emailId);

        et_addressline = (MaterialEditText)findViewById(R.id.et_addressline);
        et_addressline1 = (MaterialEditText)findViewById(R.id.et_addressline1);
        et_city = (MaterialEditText)findViewById(R.id.et_city);
        et_pincode = (MaterialEditText)findViewById(R.id.et_pincode);
        et_idnumber = (MaterialEditText)findViewById(R.id.et_idnumber);

        txt_dateofbirth = (TextView)findViewById(R.id.txt_dob);
        idexpiry = (TextView)findViewById(R.id.txt_idexpiry);


        titleSpinner = (Spinner)findViewById(R.id.titleSpinner);
        genderSpinner = (Spinner)findViewById(R.id.genderSpinner);
        cardtypeSpinner = (Spinner)findViewById(R.id.cardtypeSpinner);
        corporateSpinner = (Spinner)findViewById(R.id.corporateSpinner);

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, android.R.id.text1);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        corporateSpinner.setAdapter(spinnerAdapter);
        spinnerAdapter.add("Select corporate");
        spinnerAdapter.add(userdataInputTo.getBusiness());
        spinnerAdapter.notifyDataSetChanged();

        stateSpinner = (Spinner)findViewById(R.id.stateSpinner);
        proofSpinner = (Spinner)findViewById(R.id.proofSpinner);
        kyctypeSpinner = (Spinner)findViewById(R.id.kyctypeSpinner);

        proofSpinner1 = (Spinner)findViewById(R.id.proofSpinner1);
        proofSpinner2 = (Spinner)findViewById(R.id.proofSpinner2);
        proofSpinner3 = (Spinner)findViewById(R.id.proofSpinner3);
        proofSpinner4 = (Spinner)findViewById(R.id.proofSpinner4);
        proofSpinner5 = (Spinner)findViewById(R.id.proofSpinner5);

        proofSpinner1.setSelection(0,false);
        proofSpinner2.setSelection(0,false);
        proofSpinner3.setSelection(0,false);
        proofSpinner4.setSelection(0,false);
        proofSpinner5.setSelection(0,false);

        proofSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( proofSpinner2.getSelectedItemPosition()== position || proofSpinner3.getSelectedItemPosition()== position || proofSpinner4.getSelectedItemPosition()== position || proofSpinner5.getSelectedItemPosition()== position){
                    proofSpinner1.setSelection(0);
                    Toast.makeText(MainActivity.this, R.string.proofalreadyselect, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        proofSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (proofSpinner1.getSelectedItemPosition()== position || proofSpinner3.getSelectedItemPosition()== position || proofSpinner4.getSelectedItemPosition()== position || proofSpinner5.getSelectedItemPosition()== position){
                    proofSpinner2.setSelection(0);
                    Toast.makeText(MainActivity.this, R.string.proofalreadyselect, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        proofSpinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (proofSpinner1.getSelectedItemPosition()== position || proofSpinner2.getSelectedItemPosition()== position  || proofSpinner4.getSelectedItemPosition()== position || proofSpinner5.getSelectedItemPosition()== position){
                    proofSpinner3.setSelection(0);
                    Toast.makeText(MainActivity.this, R.string.proofalreadyselect, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        proofSpinner4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (proofSpinner1.getSelectedItemPosition()== position || proofSpinner2.getSelectedItemPosition()== position || proofSpinner3.getSelectedItemPosition()== position || proofSpinner5.getSelectedItemPosition()== position){
                    proofSpinner4.setSelection(0);
                    Toast.makeText(MainActivity.this, R.string.proofalreadyselect, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        proofSpinner5.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (proofSpinner1.getSelectedItemPosition()== position || proofSpinner2.getSelectedItemPosition()== position || proofSpinner3.getSelectedItemPosition()== position || proofSpinner4.getSelectedItemPosition()== position){
                    proofSpinner5.setSelection(0);
                    Toast.makeText(MainActivity.this, R.string.proofalreadyselect, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        customerreg1 = (CardView)findViewById(R.id.customerreg1);
        customerreg2 = (CardView)findViewById(R.id.customerreg2);
        customerreg3 = (CardView)findViewById(R.id.customerreg3);
        customerreg4 = (CardView)findViewById(R.id.customerreg4);

        gotoekyc = (CardView)findViewById(R.id.gotoekyc);
        gotoekyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             // startActivity(new Intent(MainActivity.this,AdduserActivity.class).putExtra("Kitno",et_kitnumber.getText().toString()));
            }
        });


        next_txt1 = (TextView)findViewById(R.id.next_txt1);
        next_txt2 = (TextView)findViewById(R.id.next_txt2);
        next_txt3 = (TextView)findViewById(R.id.next_txt3);

        back_txt1 = (TextView)findViewById(R.id.back_txt1);
        back_txt2 = (TextView)findViewById(R.id.back_txt2);
        back_txt3 = (TextView)findViewById(R.id.back_txt3);

        text_proof1 = (TextView)findViewById(R.id.text_proof1);
        text_proof2 = (TextView)findViewById(R.id.text_proof2);
        text_proof3 = (TextView)findViewById(R.id.text_proof3);
        text_proof4 = (TextView)findViewById(R.id.text_proof4);
        text_proof5 = (TextView)findViewById(R.id.text_proof5);

        signup1 = (TextView)findViewById(R.id.signup1);
        signup2 = (TextView)findViewById(R.id.signup2);
        signup3 = (TextView)findViewById(R.id.signup3);
        signup4 = (TextView)findViewById(R.id.signup4);
        signup5 = (TextView)findViewById(R.id.signup5);


        signaturePad = (SignaturePad)findViewById(R.id.signature_pad);
        clear = (TextView)findViewById(R.id.clear);

        prooflinearlay1 = (LinearLayout) findViewById(R.id.prooflinearlay1);
        prooflinearlay2 = (LinearLayout) findViewById(R.id.prooflinearlay2);
        prooflinearlay3 = (LinearLayout) findViewById(R.id.prooflinearlay3);
        prooflinearlay4 = (LinearLayout) findViewById(R.id.prooflinearlay4);
        prooflinearlay5 = (LinearLayout) findViewById(R.id.prooflinearlay5);

        signupprooflay1 = (LinearLayout) findViewById(R.id.signupprooflay1);
        signupprooflay2 = (LinearLayout) findViewById(R.id.signupprooflay2);
        signupprooflay3 = (LinearLayout) findViewById(R.id.signupprooflay3);
        signupprooflay4 = (LinearLayout) findViewById(R.id.signupprooflay4);
        signupprooflay5 = (LinearLayout) findViewById(R.id.signupprooflay5);


        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signaturePad.clear();
            }
        });


        txt_dateofbirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Date date;

                    Calendar c = Calendar.getInstance();
                    c.add(Calendar.YEAR,-18);
                    if (txt_dateofbirth.getText().toString().length()==0) {
                        String currentDateTimeString = Constants.dateFormat.format(c.getTime());
                        date = Constants.dateFormat.parse(currentDateTimeString);
                    } else {
                        date = Constants.dateFormat.parse(txt_dateofbirth.getText().toString());
                    }
                    c.setTime(date);
                    DialogFragment picker = new DatePickerDialogFragment(txt_dateofbirth, c,"dob");
                    picker.show(getSupportFragmentManager(),"Date of birth");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        idexpiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Date date;

                    Calendar c = Calendar.getInstance();
                    //c.add(Calendar.YEAR,);
                    if (idexpiry.getText().toString().length()==0) {
                        String currentDateTimeString = Constants.dateFormat.format(c.getTime());
                        date = Constants.dateFormat.parse(currentDateTimeString);
                    } else {
                        date = Constants.dateFormat.parse(idexpiry.getText().toString());
                    }
                    c.setTime(date);
                    DialogFragment picker = new DatePickerDialogFragment(idexpiry, c,"idexpiry");
                    picker.show(getSupportFragmentManager(),"ID Expiry");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        next_txt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validatecustomerdetail1()) {
                  checkUserExists();
                }
            }
        });

        next_txt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validatecustomerdetail2()) {
                    adduserrequestTO.setState(stateSpinner.getSelectedItem().toString());
                    adduserrequestTO.setAddress_Line1(et_addressline.getText().toString());
                    adduserrequestTO.setAddress_Line2(et_addressline1.getText().toString());
                    adduserrequestTO.setCity(et_city.getText().toString());
                    adduserrequestTO.setPincode(et_pincode.getText().toString());
                    adduserrequestTO.setCountry("India");
                    customerreg2.setVisibility(View.GONE);
                    customerreg3.setVisibility(View.VISIBLE);
                }
            }
        });

        next_txt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validatecustomerdetail3()) {
                    if (proofSpinner.getSelectedItemPosition()== 4) {
                        adduserrequestTO.setId_Type("DL");
                    } else {
                        adduserrequestTO.setId_Type(proofSpinner.getSelectedItem().toString());
                    }
                    adduserrequestTO.setId_Number(et_idnumber.getText().toString());
                    adduserrequestTO.setExpiry_Date(idexpiry.getText().toString());
                    adduserrequestTO.setKYC_Status(kyctypeSpinner.getSelectedItem().toString());
                    adduserrequestTO.setUserId("santhos@m2p.in");

                    customerreg3.setVisibility(View.GONE);
                    customerreg4.setVisibility(View.VISIBLE);
                }
            }
        });


        back_txt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerreg2.setVisibility(View.GONE);
                customerreg1.setVisibility(View.VISIBLE);
            }
        });

        back_txt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerreg3.setVisibility(View.GONE);
                customerreg2.setVisibility(View.VISIBLE);
            }
        });

        back_txt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerreg4.setVisibility(View.GONE);
                customerreg3.setVisibility(View.VISIBLE);
            }
        });

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proofphoto="proof1";
                getUploadpicOption();
            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proofphoto="proof2";
                getUploadpicOption();
            }
        });

        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proofphoto="proof3";
                getUploadpicOption();
            }
        });
        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proofphoto="proof4";
                getUploadpicOption();
            }
        });
        imageView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proofphoto="proof5";
                getUploadpicOption();
            }
        });


        signup1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usercheckonce) {
                    addUser();
                } else {
                    showUploadeddialog(MainActivity.this);
                }
            }
        });

        signup2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usercheckonce) {
                    addUser();
                } else {
                    showUploadeddialog(MainActivity.this);
                }
            }
        });

        signup3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usercheckonce) {
                    addUser();
                } else {
                    showUploadeddialog(MainActivity.this);
                }
            }
        });

        signup4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usercheckonce) {
                    addUser();
                } else {
                    showUploadeddialog(MainActivity.this);
                }
            }
        });

        signup5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usercheckonce) {
                    addUser();
                } else {
                    showUploadeddialog(MainActivity.this);
                }
            }
        });

        text_proof1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupprooflay1.setVisibility(View.GONE);
                prooflinearlay2.setVisibility(View.VISIBLE);
            }
        });

        text_proof2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupprooflay2.setVisibility(View.GONE);
                prooflinearlay3.setVisibility(View.VISIBLE);
            }
        });

        text_proof3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupprooflay3.setVisibility(View.GONE);
                prooflinearlay4.setVisibility(View.VISIBLE);
            }
        });

        text_proof4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupprooflay4.setVisibility(View.GONE);
                text_proof5.setVisibility(View.GONE);
                prooflinearlay5.setVisibility(View.VISIBLE);
            }
        });

        text_proof5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        System.out.println("The menu list pref are"+ Constants.getSharedPreferenceStringList(this,Constants.MENU_KEY).size());
        final Menu menu = navigationView.getMenu();
        try {
            /*for (int i = 0; i < Constants.getSharedPreferenceStringList(this,Constants.MENU_KEY).size(); i++) {
                menu.add(Constants.getSharedPreferenceStringList(this,Constants.MENU_KEY).get(i)).setIcon(myImageList[i]);
            }*/
            menu.add("Kyc list").setIcon(myImageList[0]);
            menu.add("Support").setIcon(myImageList[1]);
            menu.add("About").setIcon(myImageList[2]);
            menu.add("Logout").setIcon(myImageList[3]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i=0;i<menu.size();i++) {
            MenuItem mi = menu.getItem(i);
            applyFontToMenuItem(mi);
        }
        toolbar.setTitle(menu.getItem(0).getTitle());

        CheckExternalPermissions();
        CheckExternalReadPermissions();

        getSupportFragmentManager().beginTransaction()
                .addToBackStack("mainactivity")
                .replace(R.id.mainlayout, new PendingKycFragment(userdataInputTo)).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
           // super.onBackPressed();
            System.out.println("fragment++"+getSupportFragmentManager().getBackStackEntryCount());

           if(getSupportFragmentManager().getBackStackEntryCount() == 2){
               getSupportFragmentManager().popBackStack();
           }
           else{
               finish();
           }

        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        String id = String.valueOf(item.getTitle());
        //toolbar.setTitle(item.getTitle());
        if (id .equals("Customer Signup")) {
            // Handle the camera action
            for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++)
            {
                getSupportFragmentManager().popBackStack();
            }
            //Toast.makeText(this, id, Toast.LENGTH_SHORT).show();
        }/*else if (id .equals("Vehicle Signup")) {
            //Toast.makeText(this, id, Toast.LENGTH_SHORT).show();
            for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++)
            {
                getSupportFragmentManager().popBackStack();
            }
            getSupportFragmentManager().beginTransaction()
                    .addToBackStack("mainactivity")
                    .add(R.id.mainlayout, new VehicleSignUpFragment()).commit();
        }*/else if (id .equals("Pending Signup")) {
            //Toast.makeText(this, id, Toast.LENGTH_SHORT).show();
            for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++)
            {
                getSupportFragmentManager().popBackStack();
            }
            getSupportFragmentManager().beginTransaction()
                    .addToBackStack("mainactivity")
                    .add(R.id.mainlayout, new PendingSignUpFragment(userdataInputTo)).commit();
        } else if (id .equals("Kyc list")){
            //Toast.makeText(this, id, Toast.LENGTH_SHORT).show();
            for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++)
            {
                getSupportFragmentManager().popBackStack();
            }
            getSupportFragmentManager().beginTransaction()
                    .addToBackStack("mainactivity")
                    .add(R.id.mainlayout, new PendingKycFragment(userdataInputTo)).commit();
        }  else if (id .equals("Capture KYC")) {
            //Toast.makeText(this, id, Toast.LENGTH_SHORT).show();
            for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++)
            {
                getSupportFragmentManager().popBackStack();
            }
            getSupportFragmentManager().beginTransaction()
                    .addToBackStack("mainactivity")
                    .add(R.id.mainlayout, new CapturekycFragment(userdataInputTo)).commit();
        }
        else if (id.equals("Logout")){
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle(R.string.app_name)
                    .setMessage(R.string.logout_txt)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Constants.deleteSharedPreferences(MainActivity.this);
                            Constants.setSharedPref(MainActivity.this, Constants.SESSION_PREFS, "logintype", "loggedout");
                            Intent intent = new Intent(MainActivity.this, SplashActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), getString(R.string.appfont));
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        System.out.println("ON ACTIVITY RESULT PICURIII++++"+picUri+requestCode);
        switch (requestCode) {
            case PICK_IMAGE_REQUEST:
                if (resultCode == RESULT_OK && imageReturnedIntent != null && imageReturnedIntent.getData() != null) {
                    if (proofphoto.equals("proof1")) {
                        picUri = getImageUrlWithAuthority(this, imageReturnedIntent.getData());
                        //Toast.makeText(this, "Imageview Has "+Constants.hasImage(imageView2), Toast.LENGTH_SHORT).show();
                        if (Constants.hasImage(imageView2)) {
                            parts.add(prepareFilePart(proofSpinner1.getSelectedItem().toString(), picUri,PICK_IMAGE_REQUEST));
                            parts.remove(parts.size()-2);
                          //  Toast.makeText(this, "parts size"+parts.size(), Toast.LENGTH_SHORT).show();
                            imageView2.setImageResource(0);
                            imageView2.setImageURI(picUri);
                        } else {
                            imageView2.setImageResource(0);
                            imageView2.setImageURI(picUri);
                            parts.add(prepareFilePart(proofSpinner1.getSelectedItem().toString(), picUri,PICK_IMAGE_REQUEST));
                          //  Toast.makeText(this, "parts size"+parts.size(), Toast.LENGTH_SHORT).show();
                        }
                    } else  if (proofphoto.equals("proof2"))  {
                        picUri1 = getImageUrlWithAuthority(this, imageReturnedIntent.getData());
                        if (Constants.hasImage(imageView3)) {
                            imageView3.setImageResource(0);
                            imageView3.setImageURI(picUri1);
                            parts.add(prepareFilePart(proofSpinner2.getSelectedItem().toString(), picUri1,PICK_IMAGE_REQUEST));
                            parts.remove(parts.size()-2);
                        } else {
                            imageView3.setImageResource(0);
                            imageView3.setImageURI(picUri1);
                            parts.add(prepareFilePart(proofSpinner2.getSelectedItem().toString(), picUri1,PICK_IMAGE_REQUEST));
                        }
                    }else  if (proofphoto.equals("proof3"))  {
                        picUri2 = getImageUrlWithAuthority(this, imageReturnedIntent.getData());
                        if (Constants.hasImage(imageView4)) {
                            imageView4.setImageResource(0);
                            imageView4.setImageURI(picUri2);
                            parts.add(prepareFilePart(proofSpinner3.getSelectedItem().toString(), picUri2,PICK_IMAGE_REQUEST));
                            parts.remove(parts.size()-2);
                        } else {
                            imageView4.setImageResource(0);
                            imageView4.setImageURI(picUri2);
                            parts.add(prepareFilePart(proofSpinner3.getSelectedItem().toString(), picUri2,PICK_IMAGE_REQUEST));
                        }
                    }else  if (proofphoto.equals("proof4"))  {
                        picUri3 = getImageUrlWithAuthority(this, imageReturnedIntent.getData());
                        if (Constants.hasImage(imageView5)) {
                            imageView5.setImageResource(0);
                            imageView5.setImageURI(picUri3);
                            parts.add(prepareFilePart(proofSpinner4.getSelectedItem().toString(), picUri3,PICK_IMAGE_REQUEST));
                            parts.remove(parts.size()-2);
                        } else {
                            imageView5.setImageResource(0);
                            imageView5.setImageURI(picUri3);
                            parts.add(prepareFilePart(proofSpinner4.getSelectedItem().toString(), picUri3,PICK_IMAGE_REQUEST));
                        }
                    }else  if (proofphoto.equals("proof5"))  {
                        picUri4 = getImageUrlWithAuthority(this, imageReturnedIntent.getData());
                        if (Constants.hasImage(imageView6)) {
                            imageView6.setImageResource(0);
                            imageView6.setImageURI(picUri4);
                            parts.add(prepareFilePart(proofSpinner5.getSelectedItem().toString(), picUri4,PICK_IMAGE_REQUEST));
                            parts.remove(parts.size()-2);
                        } else {
                            imageView6.setImageResource(0);
                            imageView6.setImageURI(picUri4);
                            parts.add(prepareFilePart(proofSpinner5.getSelectedItem().toString(), picUri4,PICK_IMAGE_REQUEST));
                        }
                    }

                }
            case REQUEST_CAMERA:
                if ((resultCode == RESULT_OK) && (requestCode == REQUEST_CAMERA)) {
                    //picUri = getPhotoFileUri(photoFileName);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    options.inJustDecodeBounds = false;
                    Bitmap bitmap = BitmapFactory.decodeFile(imageFilePath,options);
                    //System.out.println("imagepath createimagefile++++"+imageFilePath);

                    if (proofphoto.equals("proof1")) {
                        if (Constants.hasImage(imageView2)) {
                            parts.add(prepareFilePart(proofSpinner1.getSelectedItem().toString(),picUri,REQUEST_CAMERA));
                            parts.remove(parts.size()-2);
                            imageView2.setImageBitmap(bitmap);
                        } else {
                            imageView2.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner1.getSelectedItem().toString(),picUri,REQUEST_CAMERA));
                        }
                    } else if (proofphoto.equals("proof2")) {
                        if (Constants.hasImage(imageView3)) {
                            imageView3.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner2.getSelectedItem().toString(),picUri1,REQUEST_CAMERA));
                            parts.remove(parts.size()-2);
                        } else {
                            imageView3.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner2.getSelectedItem().toString(),picUri1,REQUEST_CAMERA));
                        }
                    } else if (proofphoto.equals("proof3")) {
                        if (Constants.hasImage(imageView4)) {
                            imageView4.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner3.getSelectedItem().toString(),picUri2,REQUEST_CAMERA));
                            parts.remove(parts.size()-2);
                        } else {
                            imageView4.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner3.getSelectedItem().toString(),picUri2,REQUEST_CAMERA));
                        }
                    } else if (proofphoto.equals("proof4")) {
                        if (Constants.hasImage(imageView5)) {
                            imageView5.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner4.getSelectedItem().toString(),picUri3,REQUEST_CAMERA));
                            parts.remove(parts.size()-2);
                        } else {
                            imageView5.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner4.getSelectedItem().toString(),picUri3,REQUEST_CAMERA));
                        }
                    } else if (proofphoto.equals("proof5")) {
                        if (Constants.hasImage(imageView6)) {
                            imageView6.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner5.getSelectedItem().toString(),picUri4,REQUEST_CAMERA));
                            parts.remove(parts.size()-2);
                        } else {
                            imageView6.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner5.getSelectedItem().toString(),picUri4,REQUEST_CAMERA));
                        }
                    }
                }

            case CustomBaseKycAdapter.LOCATIONACT_ID:
                System.out.println("locationspermission"+resultCode);
                if(resultCode == RESULT_OK){
                    Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
                }
                else{
                    Constants.showCustomDialog(this,"Turn on location");
                }
        }
    }

    public static Uri getImageUrlWithAuthority(Context context, Uri uri) {
        InputStream is = null;
        if (uri.getAuthority() != null) {
            try {
                is = context.getContentResolver().openInputStream(uri);
                Bitmap bmp = BitmapFactory.decodeStream(is);
                return writeToTempImageAndGetPathUri(context, bmp);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static Uri writeToTempImageAndGetPathUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "KYC", null);
        //System.out.println("kyc img path"+path);

        return Uri.parse(path);
    }



    public boolean validatecustomerdetail1(){

        if (et_firstname.getText().toString().length()==0){
            et_firstname.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (et_lastname.getText().toString().length()==0){
            et_lastname.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (et_mobileNumber.getText().toString().length()==0){
            et_mobileNumber.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (et_mobileNumber.getText().toString().length() < 10){
            et_mobileNumber.setError(getString(R.string.mobilevalidation));
            return false;
        }
        else if (et_customerId.getText().toString().length()==0){
            et_customerId.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (et_kitnumber.getText().toString().length()==0){
            et_kitnumber.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (et_kitnumber.getText().toString().length() < 9 ){
            et_kitnumber.setError(getString(R.string.kitvalidation));
            return false;
        }
        else if (et_mailId.getText().toString().length()==0){
            et_mailId.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (txt_dateofbirth.getText().toString().length()==0){
            Toast.makeText(this, R.string.emptydob_txt, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(titleSpinner.getSelectedItemPosition()==0){
            Toast.makeText(this, R.string.emptytitle, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(genderSpinner.getSelectedItemPosition()==0){
            Toast.makeText(this, R.string.emptygender, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(cardtypeSpinner.getSelectedItemPosition()==0){
            Toast.makeText(this, R.string.emptycardtype, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(corporateSpinner.getSelectedItemPosition()==0){
            Toast.makeText(this, R.string.emptycorporate, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }



    public boolean validatecustomerdetail2(){
        if(stateSpinner.getSelectedItemPosition()==0){
            Toast.makeText(this, R.string.stateempty_txt, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (et_addressline.getText().toString().length()==0){
            et_addressline.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if(et_addressline1.getText().toString().length()==0){
            et_addressline1.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if(et_city.getText().toString().length()==0){
            et_city.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if(et_pincode.getText().toString().length()==0){
            et_pincode.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (et_pincode.getText().toString().length() < 6 ){
            et_pincode.setError(getString(R.string.pincodevalidation));
            return false;
        }

        return true;
    }

    public boolean validatecustomerdetail3(){
        if(proofSpinner.getSelectedItemPosition()==0){
            Toast.makeText(this, R.string.id_typeempty, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(kyctypeSpinner.getSelectedItemPosition()==0){
            Toast.makeText(this, R.string.kyc_typeempty, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(et_idnumber.getText().toString().length()==0){
            et_idnumber.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if(idexpiry.getText().toString().length()==0){
            Toast.makeText(this, R.string.emptyexpiry_txt, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri,int from) {
        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
        File file;
        if (from == PICK_IMAGE_REQUEST) {
            file = FileUtils.getFile(this, fileUri);
           // System.out.println("file path"+file.getAbsolutePath());
        } else {
            file = getFilePathFromURI(this,fileUri);
            //System.out.println("file path"+file.getAbsolutePath());
        }

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(this.getContentResolver().getType(fileUri)),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("MainActivity","onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d("MainActivity","OnRestore");
    }

    public void addUser(){
        Constants.showLoading(this);

        final String json = new Gson().toJson(adduserrequestTO);
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        //loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                String token = "Basic "+Constants.serviceUrls.SESSIONTOKEN;
                //System.out.println("requestInterceptor main"+token);

                Request original = chain.request();

                Request.Builder builder = original.newBuilder();
                builder.header("Content-Type","multipart/form-data");
                builder.header("Authorization",token);


                Request request = builder.method(original.method(), original.body())
                        .build();

                Log.e("header in main",request.header("Authorization"));
                Log.e("header in main",request.header("Content-Type"));
                return chain.proceed(request);
            }
        });
        httpClient.addInterceptor(loggingInterceptor);


        Retrofit client = new Retrofit.Builder()
                .baseUrl(BuildConfig.KycBaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        RequestBody jsonstring = createPartFromString(json);
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("jsonBody",jsonstring);
        //System.out.println("Content type++++"+jsonstring.contentType()+jsonstring.toString());

        GetUsersServiceAPI getserviceApi = client.create(GetUsersServiceAPI.class);
        getserviceApi.adduser(map,parts).enqueue(new retrofit2.Callback<AgentLoginResponseTO>() {
            @Override
            public void onResponse(Call<AgentLoginResponseTO> call, retrofit2.Response<AgentLoginResponseTO> response) {
                Constants.dismissloading();
                //Toast.makeText(MainActivity.this, "Successfully registered!", Toast.LENGTH_SHORT).show();
                if (response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Successfully registered!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    try {
                        Gson gson = new Gson();
                        ResponseTO responseTO = gson.fromJson(response.errorBody().charStream(), ResponseTO.class);
                        Toast.makeText(MainActivity.this, responseTO.getException().getDetailMessage(), Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(MainActivity.this, "Proxy server error", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<AgentLoginResponseTO> call, Throwable t) {
                Constants.dismissloading();
                //System.out.println("Erorrrr"+t.getMessage());
                Toast.makeText(MainActivity.this, "Error in upload", Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void getCameraPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            }
        }
    }

    public void checkUserExists(){
        Constants.getEkycServicesAPI(this,corporateSpinner.getSelectedItem().toString()).checkuserExists(et_customerId.getText().toString(), new Callback<AgentLoginResponseTO>() {
            @Override
            public void success(AgentLoginResponseTO agentLoginResponseTO, Response response) {
                Toast.makeText(MainActivity.this, R.string.userexists, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                adduserrequestTO.setStatus(Integer.parseInt(userdataInputTo.getStatus()));
                adduserrequestTO.setAuthLevel(Integer.parseInt(userdataInputTo.getAuthLevel()));
                adduserrequestTO.setUserLevel("");
                adduserrequestTO.setBusiness(userdataInputTo.getBusiness());
                adduserrequestTO.setBank(userdataInputTo.getBankName());
                adduserrequestTO.setFirst_Name(et_firstname.getText().toString());
                adduserrequestTO.setLast_Name(et_lastname.getText().toString());
                adduserrequestTO.setContact_No(et_mobileNumber.getText().toString());
                adduserrequestTO.setCustomerId(et_customerId.getText().toString());
                adduserrequestTO.setKit_No(et_kitnumber.getText().toString());
                adduserrequestTO.setEmail_Address(et_mailId.getText().toString());
                adduserrequestTO.setDate_Of_Birth(txt_dateofbirth.getText().toString());
                adduserrequestTO.setTitle(titleSpinner.getSelectedItem().toString());
                if (genderSpinner.getSelectedItem().toString().equals("Male")) {
                    adduserrequestTO.setGender("M");
                } else {
                    adduserrequestTO.setGender("F");
                }
                if (cardtypeSpinner.getSelectedItem().toString().equals("Physical")) {
                    adduserrequestTO.setCard_Type("P");
                } else {
                    adduserrequestTO.setCard_Type("V");
                }
                adduserrequestTO.setCorporate_Id(corporateSpinner.getSelectedItem().toString());

                customerreg1.setVisibility(View.GONE);
                customerreg2.setVisibility(View.VISIBLE);
            }
        });
    }

    private void CheckExternalPermissions() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
            }
        }
    }

    public void getUploadpicOption(){
        CheckExternalPermissions();
        CheckExternalReadPermissions();
        getCameraPermission();
       /* StrictMode.VmPolicy.Builder strictbuilder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(strictbuilder.build());
*/
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            Intent pictureIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);

            if (pictureIntent.resolveActivity(getPackageManager()) != null) {
                //Create a file to store the image
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File
                }
                if (photoFile != null) {
                    if (proofphoto.equals("proof1")) {
                        picUri = FileProvider.getUriForFile(MainActivity.this, "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri);
                    } else if (proofphoto.equals("proof2")) {
                        picUri1 = FileProvider.getUriForFile(MainActivity.this, "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri1);
                    }else if (proofphoto.equals("proof3")) {
                        picUri2 = FileProvider.getUriForFile(MainActivity.this, "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri2);
                    }else if (proofphoto.equals("proof4")) {
                        picUri3 = FileProvider.getUriForFile(MainActivity.this, "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri3);
                    }else if (proofphoto.equals("proof5")) {
                        picUri4 = FileProvider.getUriForFile(MainActivity.this, "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri4);
                    }
                    startActivityForResult(pictureIntent,
                            REQUEST_CAMERA);
                }
            }

        } else {
            // Toast.makeText(getActivity(), "You dont have permission to access Camera!", Toast.LENGTH_LONG).show();
            getCameraPermission();
        }

     /*   final CharSequence[] items = {getString(R.string.takePhto), getString(R.string.fromGallery), getString(R.string.cancel)};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.takePhto))) {
                    if (ContextCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.CAMERA)
                            == PackageManager.PERMISSION_GRANTED) {
                        Intent pictureIntent = new Intent(
                                MediaStore.ACTION_IMAGE_CAPTURE);

                        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
                            //Create a file to store the image
                            File photoFile = null;
                            try {
                                photoFile = createImageFile();
                            } catch (IOException ex) {
                                // Error occurred while creating the File
                            }
                            if (photoFile != null) {
                                if (proofphoto.equals("proof1")) {
                                    picUri = FileProvider.getUriForFile(MainActivity.this, "com.anto.kycapp.provider", photoFile);
                                    pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                            picUri);
                                } else if (proofphoto.equals("proof2")) {
                                    picUri1 = FileProvider.getUriForFile(MainActivity.this, "com.anto.kycapp.provider", photoFile);
                                    pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                            picUri1);
                                }else if (proofphoto.equals("proof3")) {
                                    picUri2 = FileProvider.getUriForFile(MainActivity.this, "com.anto.kycapp.provider", photoFile);
                                    pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                            picUri2);
                                }else if (proofphoto.equals("proof4")) {
                                    picUri3 = FileProvider.getUriForFile(MainActivity.this, "com.anto.kycapp.provider", photoFile);
                                    pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                            picUri3);
                                }else if (proofphoto.equals("proof5")) {
                                    picUri4 = FileProvider.getUriForFile(MainActivity.this, "com.anto.kycapp.provider", photoFile);
                                    pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                            picUri4);
                                }
                                startActivityForResult(pictureIntent,
                                        REQUEST_CAMERA);
                            }
                        }

                    } else {
                        // Toast.makeText(getActivity(), "You dont have permission to access Camera!", Toast.LENGTH_LONG).show();
                        getCameraPermission();
                    }

                } else if (items[item].equals(getString(R.string.fromGallery))) {
                    if (ContextCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent(
                                Intent.ACTION_GET_CONTENT,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image*//*");
                        intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        startActivityForResult(
                                Intent.createChooser(intent, "Select File"), PICK_IMAGE_REQUEST);
                    } else {
                        //Toast.makeText(getActivity(), "You dont have permission to Read Data!", Toast.LENGTH_LONG).show();
                        CheckExternalReadPermissions();
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();*/
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void CheckExternalReadPermissions() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_READ);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_READ);
            }
        }
    }
    private boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }

    private File createImageFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        imageFileName = photoFileName + timeStamp + "_";
        File storageDir =
               getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();

        return image;
    }


    public Uri getPhotoFileUri(String fileName) {
        if (isExternalStorageAvailable()) {
            File mediaStorageDir = new File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);
            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
//                Log.d(APP_TAG, "failed to create directory");
            }
            //return Uri.fromFile(new File(mediaStorageDir.getPath() + File.separator + fileName));
           return FileProvider.getUriForFile(this, "com.anto.kycapp.provider", new File(mediaStorageDir.getPath() + File.separator + fileName));
        }
        return null;
    }


    public  File getFilePathFromURI(Context context, Uri contentUri) {
        //copy file and send new file path
        String fileName = getFileName(contentUri);
        if (!TextUtils.isEmpty(fileName)) {
            File mediaStorageDir = new File(MainActivity.this.getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);
            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
//                Log.d(APP_TAG, "failed to create directory");
            }
            File copyFile = new File(mediaStorageDir.getPath() + File.separator + fileName);
            copy(context, contentUri, copyFile);
            return copyFile;
        }
        return null;
    }

    public static String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IoUtils.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showUploadeddialog(final Activity activity){
        final Dialog uploadeddialog = new Dialog(activity,android.R.style.Theme_Material_Dialog_Alert);
        uploadeddialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        uploadeddialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        uploadeddialog.setCancelable(false);
        uploadeddialog.setContentView(R.layout.signupalert_view);

        TextView yes = uploadeddialog.findViewById(R.id.textYes);
        TextView signup = uploadeddialog.findViewById(R.id.textSignmeUp);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerreg4.setVisibility(View.GONE);
                customerreg1.setVisibility(View.VISIBLE);
                usercheckonce = true;
                uploadeddialog.dismiss();
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadeddialog.dismiss();
                addUser();
            }
        });

        uploadeddialog.show();
    }


    public static boolean reduceImage(String path, long maxSize) {
        File img = new File(path);
        boolean result = false;
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bitmap = null;
        options.inSampleSize=1;
        while (img.length()>maxSize) {
            options.inSampleSize = options.inSampleSize+1;
            bitmap = BitmapFactory.decodeFile(path, options);
            img.delete();
            try
            {
                FileOutputStream fos = new FileOutputStream(path);
                bitmap.compress(path.toLowerCase().endsWith("jpg")?
                        Bitmap.CompressFormat.JPEG:
                        Bitmap.CompressFormat.JPEG, 100, fos);
                fos.close();
                result = true;
            }catch (Exception errVar) {
                errVar.printStackTrace();
            }
        }
        return result;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CustomBaseKycAdapter.PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CustomBaseKycAdapter customBaseKycAdapter = new CustomBaseKycAdapter();
                customBaseKycAdapter.displayLocationSettingsRequest(this);

                Toast.makeText(this,"Permission granted",Toast.LENGTH_LONG).show();
            }
            else{
                Constants.showCustomDialog(this, "Kindly give permission..");
            }
        }
    }



}
