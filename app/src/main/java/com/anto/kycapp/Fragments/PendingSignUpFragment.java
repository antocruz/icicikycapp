package com.anto.kycapp.Fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.anto.kycapp.Adapter.CustomBaseAdapter;
import com.anto.kycapp.BuildConfig;
import com.anto.kycapp.Interface.EkycServiceApi;
import com.anto.kycapp.Interface.GetUsersServiceAPI;
import com.anto.kycapp.Models.PendingSignupResponseTo;
import com.anto.kycapp.Models.PendingsignupResponse;
import com.anto.kycapp.Models.TransactionModel;
import com.anto.kycapp.Models.UserdataInputTo;
import com.anto.kycapp.R;
import com.anto.kycapp.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class PendingSignUpFragment extends Fragment {
    UserdataInputTo userdataInputTo;
    CustomBaseAdapter customBaseAdapter;
    ListView lvDetail;
    ArrayList<PendingsignupResponse> transactionList = new ArrayList<>();
    public  static String[] title = new String[]{
            "Exchange from INR", "Exchange from USD", "Exchange from USD", "Exchange from INR","Exchange from INR", "Exchange from USD", "Exchange from USD"
    };
    public PendingSignUpFragment(){}

    @SuppressLint("ValidFragment")
    public PendingSignUpFragment(UserdataInputTo userdataInputTo) {
        this.userdataInputTo = userdataInputTo;
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pending_sign_up, container, false);
        lvDetail = (ListView)view.findViewById(R.id.listview);
        getDataInList();
        customBaseAdapter=new CustomBaseAdapter(getActivity(), transactionList);
        lvDetail.setAdapter(customBaseAdapter);


        return view;
    }

    private void getDataInList() {
        //custom okHttp3 api
        Constants.showLoading(getActivity());
        Constants.getUsersAPI(getActivity()).fetchPendingSignup(userdataInputTo.getBusiness(),userdataInputTo.getAuthLevel(),userdataInputTo.getStatus(),userdataInputTo.getUserType(), new Callback<PendingSignupResponseTo>() {
            @Override
            public void success(PendingSignupResponseTo pendingSignupResponseTo, Response response) {
                Constants.dismissloading();

                for (int i = 0; i < pendingSignupResponseTo.getResults().size(); i++) {

                    //try {
                        PendingsignupResponse ld = new PendingsignupResponse();
                        ld.setBusiness(pendingSignupResponseTo.getResults().get(i).getBusiness());
                        ld.setCustomerId(pendingSignupResponseTo.getResults().get(i).getCustomerId());
                        ld.setId(pendingSignupResponseTo.getResults().get(i).getId());
                        ld.setCreated(pendingSignupResponseTo.getResults().get(i).getCreated());
                        ld.setStatus(pendingSignupResponseTo.getResults().get(i).getStatus());
                        transactionList.add(ld);

                    //}
                    /*catch (Exception e) {
                        e.printStackTrace();
                    }*/
                }
                customBaseAdapter.notifyDataSetChanged();

            }

            @Override
            public void failure(RetrofitError error) {
                Constants.dismissloading();
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

/*
        for (int i = 0; i < title.length; i++) {
            // Create a new object for each list item
            PendingSignupResponseTo ld = new PendingSignupResponseTo();
            ld.setStatus(title[i]);

            // Add this object into the ArrayList myList
            transactionList.add(ld);
        }*/
    }

}
