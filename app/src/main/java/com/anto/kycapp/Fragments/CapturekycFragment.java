package com.anto.kycapp.Fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anto.kycapp.AdduserActivity;
import com.anto.kycapp.BuildConfig;
import com.anto.kycapp.Interface.GetUsersServiceAPI;
import com.anto.kycapp.LoginActivity;
import com.anto.kycapp.MainActivity;
import com.anto.kycapp.Models.AadharValidationResponseTo;
import com.anto.kycapp.Models.AgentLoginResponseTO;
import com.anto.kycapp.Models.CaptureKycDatainputTo;
import com.anto.kycapp.Models.OtpResponseTo;
import com.anto.kycapp.Models.PanValidationResponseTo;
import com.anto.kycapp.Models.ResponseTO;
import com.anto.kycapp.Models.UserdataInputTo;
import com.anto.kycapp.Models.ValidatecardandkitRequestTo;
import com.anto.kycapp.Models.ValidatecardandkitResponseTo;
import com.anto.kycapp.Models.ValidationRequestTo;
import com.anto.kycapp.R;
import com.anto.kycapp.Utils.Constants;
import com.anto.kycapp.Utils.FileUtils;
import com.anto.kycapp.Utils.FontChangeCrawler;
import com.anto.kycapp.Utils.IoUtils;
import com.google.gson.Gson;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class CapturekycFragment extends Fragment {
    MaterialEditText kitnumber, cardlastfour, mobileNumber, customername,etpanvalid,entityid,etaadhaarvalid,aadhaarentityid,et_aadhaarotpref,et_aadhaarotp;
    ImageView customerphoto, imageView2, imageView3, imageView4, imageView5, imageView6;
    TextView text_proof1, text_proof2, text_proof3, text_proof4, text_proof5,validate,pansubmit,aadhaarsubmit;
    TextView uploadkyc1,uploadkyc2,uploadkyc3,uploadkyc4,uploadkyc5;
    public static final int EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 2;
    LinearLayout prooflinearlay1, prooflinearlay2, prooflinearlay3, prooflinearlay4, prooflinearlay5;
    LinearLayout signupprooflay1, signupprooflay2, signupprooflay3, signupprooflay4, signupprooflay5,uploadkyclay;
    private Uri cuspicUri,picUri,picUri1,picUri2,picUri3,picUri4;
    List<MultipartBody.Part> parts = new ArrayList<>();
    String imageFilePath, imageFileName;
    String photoFileName = "kycdocument", proofphoto = "";
    int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    CardView gotoekyc;
    protected static final int REQUEST_CAMERA = 0;
    UserdataInputTo userdataInputTo;
    public CaptureKycDatainputTo captureKycDatainputTo = new CaptureKycDatainputTo();
    protected static final int PICK_IMAGE_REQUEST = 1;
    public CapturekycFragment() {
        // Required empty public constructor
    }
    File photoFile = null;
    @SuppressLint("ValidFragment")
    public CapturekycFragment(UserdataInputTo userdataInputTo) {
        this.userdataInputTo = userdataInputTo;
        // Required empty public constructor
    }
    Spinner proofSpinner1,proofSpinner2,proofSpinner3,proofSpinner4,proofSpinner5;
     public  int clickcount = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_capturekyc, container, false);
        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), getActivity().getResources().getString(R.string.appfont));
        fontChanger.replaceFonts((ViewGroup) view.findViewById(R.id.capturekyclay));

        kitnumber    = (MaterialEditText) view.findViewById(R.id.kitnumber);
        cardlastfour = (MaterialEditText) view.findViewById(R.id.cardlastfour);
        mobileNumber = (MaterialEditText) view.findViewById(R.id.mobileNumber);
        customername = (MaterialEditText) view.findViewById(R.id.customername);

        etpanvalid = (MaterialEditText) view.findViewById(R.id.etpanvalid);
        entityid = (MaterialEditText) view.findViewById(R.id.entityid);

        etaadhaarvalid = (MaterialEditText) view.findViewById(R.id.etaadhaarvalid);
        aadhaarentityid = (MaterialEditText) view.findViewById(R.id.aadhaarentityid);
        et_aadhaarotp = (MaterialEditText) view.findViewById(R.id.et_aadhaarotp);
        et_aadhaarotpref = (MaterialEditText) view.findViewById(R.id.et_aadhaarotpref);

        gotoekyc = (CardView)view.findViewById(R.id.gotoekyc);
        customerphoto = (ImageView) view.findViewById(R.id.customerImg);

        imageView2 = (ImageView) view.findViewById(R.id.capture_imageView2);
        imageView3 = (ImageView) view.findViewById(R.id.capture_imageView3);
        imageView4 = (ImageView) view.findViewById(R.id.capture_imageView4);
        imageView5 = (ImageView) view.findViewById(R.id.capture_imageView5);
        imageView6 = (ImageView) view.findViewById(R.id.capture_imageView6);

        prooflinearlay1 = (LinearLayout) view.findViewById(R.id.prooflinearlay1);
        prooflinearlay2 = (LinearLayout) view.findViewById(R.id.prooflinearlay2);
        prooflinearlay3 = (LinearLayout) view.findViewById(R.id.prooflinearlay3);
        prooflinearlay4 = (LinearLayout) view.findViewById(R.id.prooflinearlay4);
        prooflinearlay5 = (LinearLayout) view.findViewById(R.id.prooflinearlay5);

        signupprooflay1 = (LinearLayout) view.findViewById(R.id.signupprooflay1);
        signupprooflay2 = (LinearLayout) view.findViewById(R.id.signupprooflay2);
        signupprooflay3 = (LinearLayout) view.findViewById(R.id.signupprooflay3);
        signupprooflay4 = (LinearLayout) view.findViewById(R.id.signupprooflay4);
        signupprooflay5 = (LinearLayout) view.findViewById(R.id.signupprooflay5);

        proofSpinner1 = (Spinner)view.findViewById(R.id.proofSpinner1);
        proofSpinner2 = (Spinner)view.findViewById(R.id.proofSpinner2);
        proofSpinner3 = (Spinner)view.findViewById(R.id.proofSpinner3);
        proofSpinner4 = (Spinner)view.findViewById(R.id.proofSpinner4);
        proofSpinner5 = (Spinner)view.findViewById(R.id.proofSpinner5);

        uploadkyclay = (LinearLayout) view.findViewById(R.id.uploadkyclay);

        text_proof1 = (TextView) view.findViewById(R.id.text_proof1);
        text_proof2 = (TextView) view.findViewById(R.id.text_proof2);
        text_proof3 = (TextView) view.findViewById(R.id.text_proof3);
        text_proof4 = (TextView) view.findViewById(R.id.text_proof4);
        text_proof5 = (TextView) view.findViewById(R.id.text_proof5);

        uploadkyc1 = (TextView)view.findViewById(R.id.signup1);
        uploadkyc2 = (TextView)view.findViewById(R.id.signup2);
        uploadkyc3 = (TextView)view.findViewById(R.id.signup3);
        uploadkyc4 = (TextView)view.findViewById(R.id.signup4);
        uploadkyc5 = (TextView)view.findViewById(R.id.signup5);


        validate = (TextView) view.findViewById(R.id.validate);
        pansubmit = (TextView) view.findViewById(R.id.pansubmit);
        aadhaarsubmit = (TextView) view.findViewById(R.id.aadhaarsubmit);

        CheckExternalPermissions();

        gotoekyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validatekit()) {
                    startActivity(new Intent(getActivity(), AdduserActivity.class).putExtra("Kitno",kitnumber.getText().toString()));
                }
            }
        });


        uploadkyc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateUploadKyc())
                    capturekyc();
            }
        });

        uploadkyc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateUploadKyc())
                    capturekyc();
            }
        });

        uploadkyc3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateUploadKyc())
                    capturekyc();
            }
        });

        uploadkyc4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateUploadKyc())
                    capturekyc();
            }
        });

        uploadkyc5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateUploadKyc())
                    capturekyc();
            }
        });

        customerphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proofphoto = "customerphoto";
                getUploadpicOption();
            }
        });

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proofphoto = "proof1";
                getUploadpicOption();
            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proofphoto = "proof2";
                getUploadpicOption();
            }
        });

        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proofphoto = "proof3";
                getUploadpicOption();
            }
        });
        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proofphoto = "proof4";
                getUploadpicOption();
            }
        });
        imageView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proofphoto = "proof5";
                getUploadpicOption();
            }
        });

        text_proof1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupprooflay1.setVisibility(View.GONE);
                prooflinearlay2.setVisibility(View.VISIBLE);
            }
        });

        text_proof2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupprooflay2.setVisibility(View.GONE);
                prooflinearlay3.setVisibility(View.VISIBLE);
            }
        });

        text_proof3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupprooflay3.setVisibility(View.GONE);
                prooflinearlay4.setVisibility(View.VISIBLE);
            }
        });

        text_proof4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupprooflay4.setVisibility(View.GONE);
                text_proof5.setVisibility(View.GONE);
                prooflinearlay5.setVisibility(View.VISIBLE);
            }
        });

        text_proof5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validatekitcard()){
                     validateCardandKit();
                }
            }
        });

        pansubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validatePan()){
                 panValidate(getActivity());
                }
            }
        });

        aadhaarsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickcount == 0) {
                    if (validateOtp()){
                        clickcount = 1;
                        et_aadhaarotp.setVisibility(View.VISIBLE);
                        et_aadhaarotpref.setVisibility(View.VISIBLE);
                        generateOtp(getActivity());
                    }
                } else {
                    if(validateOtp1()){
                       validateOtp(getActivity());
                    }
                }
            }
        });

        //uploadkyclay.setVisibility(View.VISIBLE);
        return view;
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
       /* File file;
        file = getFilePathFromURI(getActivity(),fileUri);*/
        //System.out.println("file path"+file.getAbsolutePath());

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(getActivity().getContentResolver().getType(fileUri)),
                        photoFile
                );

        // MultipartBody.Part is used to send also the actual file name
        //System.out.println("filename"+photoFile.getName());
        return MultipartBody.Part.createFormData(partName, photoFile.getName(), requestFile);
    }


    private void panValidate(final Activity activity){
        Constants.showLoading(activity);
        ValidationRequestTo validationRequestTo = new ValidationRequestTo();
        validationRequestTo.setIdType("PAN");
        validationRequestTo.setIdNumber(etpanvalid.getText().toString());
        validationRequestTo.setEntityId(entityid.getText().toString());

        Constants.getValidateApi(activity).validatePan(validationRequestTo, new Callback<PanValidationResponseTo>() {
            @Override
            public void success(PanValidationResponseTo panValidationResponseTo, Response response) {
                Constants.dismissloading();

                if (panValidationResponseTo.getBody().getResult().getValidationResponse().isValid()) {
                    Constants.showCustomDialog(activity, "PAN validation success");
                } else {
                    Constants.showCustomDialog(activity, "PAN validation failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void generateOtp(final Activity activity){
        Constants.showLoading(activity);
        ValidationRequestTo validationRequestTo = new ValidationRequestTo();
        validationRequestTo.setIdType("AADHAR");
        validationRequestTo.setIdNumber(etaadhaarvalid.getText().toString());
        validationRequestTo.setEntityId(aadhaarentityid.getText().toString());
        validationRequestTo.setIdTypeValidation("U");

        Constants.getValidateApi(activity).generateAadhaarOtp(validationRequestTo, new Callback<AadharValidationResponseTo>() {
            @Override
            public void success(AadharValidationResponseTo aadharValidationResponseTo, Response response) {
                Constants.dismissloading();
                Constants.showCustomDialog(activity,aadharValidationResponseTo.getBody().getResult().getValidationResponse().getDescription());
            }

            @Override
            public void failure(RetrofitError error) {
                Constants.dismissloading();
            }
        });
    }

    private void validateOtp(final Activity activity){
        Constants.showLoading(activity);
        ValidationRequestTo validationRequestTo = new ValidationRequestTo();
        validationRequestTo.setIdNumber(etaadhaarvalid.getText().toString());
        validationRequestTo.setEntityId(aadhaarentityid.getText().toString());
        validationRequestTo.setIdTypeValidation("U");
        validationRequestTo.setOtpReferenceNo(et_aadhaarotpref.getText().toString());
        validationRequestTo.setOtp(et_aadhaarotp.getText().toString());

        Constants.getValidateApi(activity).validateAadhaarOtp(validationRequestTo, new Callback<OtpResponseTo>() {
            @Override
            public void success(OtpResponseTo otpResponseTo, Response response) {
                Constants.dismissloading();
                Constants.toastMessage(activity,"Success");
            }

            @Override
            public void failure(RetrofitError error) {
                Constants.dismissloading();
            }
        });
    }


    public void capturekyc(){
        Constants.showLoading(getActivity());
      /*  final Handler handler = new Handler();
        final Runnable waitingTread = new Runnable() {
            public void run() {
               Constants.dismissloading();
               Constants.showUploadeddialog(getActivity());
            }
        };
        handler.postDelayed(waitingTread, 3000);*/
        captureKycDatainputTo.setBankName(userdataInputTo.getBankName());
        captureKycDatainputTo.setBusiness(userdataInputTo.getBusiness());
        captureKycDatainputTo.setAuthLevel(userdataInputTo.getAuthLevel());
        captureKycDatainputTo.setStatus(userdataInputTo.getStatus());
        captureKycDatainputTo.setUserType(userdataInputTo.getUserType());
        captureKycDatainputTo.setCorporate(userdataInputTo.getBusiness());
        captureKycDatainputTo.setUserId("kycUserMaker@m2p.in");
        captureKycDatainputTo.setMobileNumber(mobileNumber.getText().toString().trim());
        captureKycDatainputTo.setLast4(cardlastfour.getText().toString().trim());
        captureKycDatainputTo.setKitNumber(kitnumber.getText().toString().trim());
        captureKycDatainputTo.setCustomerName(customername.getText().toString().trim());

        final String json = new Gson().toJson(captureKycDatainputTo);
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        //loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                String token = "Basic "+Constants.serviceUrls.SESSIONTOKEN;
                //System.out.println("requestInterceptor main"+token);

                Request original = chain.request();

                Request.Builder builder = original.newBuilder();
                builder.header("Content-Type","multipart/form-data");
                builder.header("Authorization",token);


                Request request = builder.method(original.method(), original.body())
                        .build();

                Log.e("header in main",request.header("Authorization"));
                Log.e("header in main",request.header("Content-Type"));
                return chain.proceed(request);
            }
        });
        httpClient.addInterceptor(loggingInterceptor);


        Retrofit client = new Retrofit.Builder()
                .baseUrl(BuildConfig.KycBaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        RequestBody jsonstring = createPartFromString(json);
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("jsonBody",jsonstring);
        //System.out.println("Content type++++"+jsonstring.contentType()+jsonstring.toString());

        GetUsersServiceAPI getserviceApi = client.create(GetUsersServiceAPI.class);
        getserviceApi.capturekyc(map,parts).enqueue(new retrofit2.Callback<AgentLoginResponseTO>() {
            @Override
            public void onResponse(Call<AgentLoginResponseTO> call, retrofit2.Response<AgentLoginResponseTO> response) {

                if(response.isSuccessful()){
                    Constants.dismissloading();
                    Constants.showUploadeddialog(getActivity());
                }
                else{
                    Constants.dismissloading();
                    try {
                        Gson gson = new Gson();
                        ResponseTO responseTO = gson.fromJson(response.errorBody().charStream(), ResponseTO.class);
                        Toast.makeText(getActivity(), responseTO.getException().getDetailMessage(), Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Proxy server error", Toast.LENGTH_SHORT).show();
                    }
                }

               // Constants.showUploadeddialog(getActivity());
            }

            @Override
            public void onFailure(Call<AgentLoginResponseTO> call, Throwable t) {
                Constants.dismissloading();
                //System.out.println("Error in upload"+t.getLocalizedMessage());
                Toast.makeText(getActivity(), "Error in upload"+t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void validateCardandKit(){
        Constants.showLoading(getActivity());
        ValidatecardandkitRequestTo validatecardandkitRequestTo = new ValidatecardandkitRequestTo();
        validatecardandkitRequestTo.setKit(kitnumber.getText().toString().trim());
        validatecardandkitRequestTo.setLast4(cardlastfour.getText().toString().trim());
        validatecardandkitRequestTo.setBusiness(userdataInputTo.getBusiness());

        Constants.getUsersAPI(getActivity()).validatecardandkit(validatecardandkitRequestTo, new Callback<ValidatecardandkitResponseTo>() {
            @Override
            public void success(ValidatecardandkitResponseTo validatecardandkitResponseTo, Response response) {
                Constants.dismissloading();
                kitnumber.setEnabled(false);
                cardlastfour.setEnabled(false);
                validate.setVisibility(View.GONE);
                uploadkyclay.setVisibility(View.VISIBLE);
            }

            @Override
            public void failure(RetrofitError error) {
                Constants.dismissloading();
                Constants.ShowErrorHandlerMessage(getActivity(),error);
                //Toast.makeText(getActivity(), "Invalid kit or card number", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void CheckExternalPermissions() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
            }
        }
    }

    public void getUploadpicOption() {

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            Intent pictureIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            if (pictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                //Create a file to store the image

                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File

                }
                if (photoFile != null) {
                    if(proofphoto.equals("customerphoto")){
                        cuspicUri = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                cuspicUri);
                    }
                    else if (proofphoto.equals("proof1")) {
                        picUri = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri);
                    } else if (proofphoto.equals("proof2")) {
                        picUri1 = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri1);
                    }else if (proofphoto.equals("proof3")) {
                        picUri2 = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri2);
                    }else if (proofphoto.equals("proof4")) {
                        picUri3 = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri3);
                    }else if (proofphoto.equals("proof5")) {
                        picUri4 = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri4);
                    }
                    startActivityForResult(pictureIntent,
                            REQUEST_CAMERA);
                }
            }

        } else {
//                      Toast.makeText(getActivity(), "You dont have permission to access Camera!", Toast.LENGTH_LONG).show();
            getCameraPermission();
        }

    }

    private void getCameraPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        imageFileName = photoFileName + timeStamp + "_";
        File storageDir =
                getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
      /*  ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        imageFilePath = image.getAbsolutePath();
        return image;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

       /* if (imageReturnedIntent!=null ) {
            Bitmap thumbnail = null;
            try {
                thumbnail = (Bitmap) imageReturnedIntent.getExtras().get("data");
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (thumbnail!=null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                picUri = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", destination);
                System.out.println("picuri kyc"+picUri);
                imageView2.setImageBitmap(thumbnail);
            }
        }*/

        //System.out.println("picuriiiiiii capturekyc+++" + picUri);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        Bitmap bitmap = BitmapFactory.decodeFile(imageFilePath,options);


        switch (requestCode) {

            case REQUEST_CAMERA:
                if (resultCode == RESULT_OK) {
                //  picUri = imageReturnedIntent.getData();
                    if(proofphoto.equals("customerphoto")){
                        if (Constants.hasImage(customerphoto)) {
                            parts.add(prepareFilePart("Customerphoto",cuspicUri));
                            parts.remove(parts.size()-2);
                            customerphoto.setImageBitmap(bitmap);
                        } else {
                            customerphoto.setImageBitmap(bitmap);
                            parts.add(prepareFilePart("Customerphoto",cuspicUri));
                        }
                    }
                    else if (proofphoto.equals("proof1")) {
                        if (Constants.hasImage(imageView2)) {
                            parts.add(prepareFilePart(proofSpinner1.getSelectedItem().toString(),picUri));
                            parts.remove(parts.size()-2);
                            imageView2.setImageBitmap(bitmap);
                        } else {
                            imageView2.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner1.getSelectedItem().toString(),picUri));
                        }
                    } else if (proofphoto.equals("proof2")) {
                        if (Constants.hasImage(imageView3)) {
                            imageView3.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner2.getSelectedItem().toString(),picUri1));
                            parts.remove(parts.size()-2);
                        } else {
                            imageView3.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner2.getSelectedItem().toString(),picUri1));
                        }
                    } else if (proofphoto.equals("proof3")) {
                        if (Constants.hasImage(imageView4)) {
                            imageView4.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner3.getSelectedItem().toString(),picUri2));
                            parts.remove(parts.size()-2);
                        } else {
                            imageView4.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner3.getSelectedItem().toString(),picUri2));
                        }
                    } else if (proofphoto.equals("proof4")) {
                        if (Constants.hasImage(imageView5)) {
                            imageView5.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner4.getSelectedItem().toString(),picUri3));
                            parts.remove(parts.size()-2);
                        } else {
                            imageView5.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner4.getSelectedItem().toString(),picUri3));
                        }
                    } else if (proofphoto.equals("proof5")) {
                        if (Constants.hasImage(imageView6)) {
                            imageView6.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner5.getSelectedItem().toString(),picUri4));
                            parts.remove(parts.size()-2);
                        } else {
                            imageView6.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner5.getSelectedItem().toString(),picUri4));
                        }
                    }
            }
        }
    }

    public boolean validatePan(){
        if(etpanvalid.getText().toString().length() == 0){
            etpanvalid.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if(entityid.getText().toString().length() == 0){
            entityid.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        return true;
    }

    public boolean validateOtp(){
        if(etaadhaarvalid.getText().toString().length() == 0){
            etaadhaarvalid.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if(aadhaarentityid.getText().toString().length() == 0){
            aadhaarentityid.setError(getString(R.string.emptyfield_txt));
            return false;
        }

        return true;
    }

    public boolean validateOtp1(){
        if(et_aadhaarotpref.getText().toString().length() == 0){
            et_aadhaarotpref.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if(et_aadhaarotp.getText().toString().length() == 0){
            et_aadhaarotp.setError(getString(R.string.emptyfield_txt));
            return false;
        }

        return true;
    }

    public boolean validatekitcard(){
        if (kitnumber.getText().toString().length()==0){
            kitnumber.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (kitnumber.getText().toString().length() < 9 ){
            kitnumber.setError(getString(R.string.kitvalidation));
            return false;
        }
        else if (cardlastfour.getText().toString().length()==0){
            cardlastfour.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (cardlastfour.getText().toString().length() < 4 ){
            cardlastfour.setError(getString(R.string.cardvalidation));
            return false;
        }

        return true;
    }

    public boolean validatekit(){
        if (kitnumber.getText().toString().length()==0){
            kitnumber.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (kitnumber.getText().toString().length() < 9 ){
            kitnumber.setError(getString(R.string.kitvalidation));
            return false;
        }

        return true;
    }

    public boolean validateUploadKyc(){
        if (mobileNumber.getText().toString().length()==0){
            mobileNumber.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (mobileNumber.getText().toString().length() < 10){
            mobileNumber.setError(getString(R.string.mobilevalidation));
            return false;
        }
        else if (customername.getText().toString().length()==0){
            customername.setError(getString(R.string.emptyfield_txt));
            return false;
        }

        return  true;
    }

    public  File getFilePathFromURI(Context context, Uri contentUri) {
        //copy file and send new file path
        String fileName = getFileName(contentUri);
        if (!TextUtils.isEmpty(fileName)) {
           /* File mediaStorageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
               Log.d("LOG", "failed to create directory");
            }*/
            File copyFile = new File(imageFilePath + File.separator + fileName);
            copy(context, contentUri, copyFile);
            return copyFile;
        }
        return null;
    }

    public static String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IoUtils.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


