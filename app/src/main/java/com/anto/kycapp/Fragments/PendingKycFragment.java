package com.anto.kycapp.Fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anto.kycapp.Adapter.CustomBaseAdapter;
import com.anto.kycapp.Adapter.CustomBaseKycAdapter;
import com.anto.kycapp.Models.AddkycAgentResponseTO;
import com.anto.kycapp.Models.FetchByKitInputTO;
import com.anto.kycapp.Models.KycdataInputTo;
import com.anto.kycapp.Models.PendingSignupResponseTo;
import com.anto.kycapp.Models.PendingkycResponse;
import com.anto.kycapp.Models.PendingkycResponseTo;
import com.anto.kycapp.Models.PendingsignupResponse;
import com.anto.kycapp.Models.UserdataInputTo;
import com.anto.kycapp.R;
import com.anto.kycapp.Utils.Constants;
import com.anto.kycapp.Utils.FontChangeCrawler;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class PendingKycFragment extends Fragment {
    UserdataInputTo userdataInputTo;
    CustomBaseKycAdapter customBaseAdapter;
    ListView lvDetail;
    Spinner statusfilter_spinner,searchbyspinner;
    CardView searcbykit;
    ArrayList<PendingkycResponse> transactionList = new ArrayList<>();

    public PendingKycFragment(){}

    @SuppressLint("ValidFragment")
    public PendingKycFragment(UserdataInputTo userdataInputTo) {
        this.userdataInputTo = userdataInputTo;
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pending_kyc, container, false);
        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), getActivity().getResources().getString(R.string.appfont));
        fontChanger.replaceFonts((ViewGroup) view.findViewById(R.id.pendingsignuplay));

        lvDetail = (ListView)view.findViewById(R.id.listview);
        statusfilter_spinner = (Spinner) view.findViewById(R.id.statusfilter_spinner);


        addFontToSpinner(getActivity(),statusfilter_spinner,getResources().getStringArray(R.array.status_array));

        searcbykit = view.findViewById(R.id.searchbykit);

        getDataInList();

        customBaseAdapter=new CustomBaseKycAdapter(getActivity(), transactionList);
        lvDetail.setAdapter(customBaseAdapter);

        statusfilter_spinner.setSelection(0,false);
        statusfilter_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                transactionList.clear();
                getDataInList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        searcbykit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchByKit(getActivity());
            }
        });

        return view;
    }

    private void getDataInList() {
        //custom okHttp3 api
        Constants.showLoading(getActivity());
        KycdataInputTo kycdataInputTo = new KycdataInputTo();
        kycdataInputTo.setBusiness(userdataInputTo.getBusiness());
        kycdataInputTo.setStatus(statusfilter_spinner.getSelectedItem().toString());
        Constants.getUsersAPI(getActivity()).fetchPendingKyc(kycdataInputTo, new Callback<PendingkycResponseTo>() {
            @Override
            public void success(PendingkycResponseTo pendingkycResponseTo, Response response) {
                Constants.dismissloading();
                for (int i = 0; i < pendingkycResponseTo.getResults().size(); i++) {

                    //try {
                        PendingkycResponse ld = new PendingkycResponse();
                        ld.setBusiness(pendingkycResponseTo.getResults().get(i).getBusiness());
                        ld.setEntityId(pendingkycResponseTo.getResults().get(i).getEntityId());
                        ld.setId(pendingkycResponseTo.getResults().get(i).getId());
                        ld.setCreated(Constants.getDateCurrentTimeZone(Long.parseLong(pendingkycResponseTo.getResults().get(i).getCreated())));
                        ld.seteKycRefNo(pendingkycResponseTo.getResults().get(i).geteKycRefNo());
                        ld.setKycStatus(pendingkycResponseTo.getResults().get(i).getKycStatus());
                        ld.setKycReason(pendingkycResponseTo.getResults().get(i).getKycReason());
                        ld.setKitNo(pendingkycResponseTo.getResults().get(i).getKitNo());
                        transactionList.add(ld);

                    //}
                    /*catch (Exception e) {
                        e.printStackTrace();
                    }*/
                }
                customBaseAdapter.notifyDataSetChanged();

            }

            @Override
            public void failure(RetrofitError error) {
                Constants.dismissloading();
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public  void searchByKit(final Activity activity){
        final Dialog dialog = new Dialog(activity,android.R.style.Theme_Material_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.kitnumber_view);

        final EditText etKit = dialog.findViewById(R.id.et_kit);
        TextView search = dialog.findViewById(R.id.submituser_txt);

        searchbyspinner =  dialog.findViewById(R.id.searchbyspinner);
        addFontToSpinner(getActivity(),searchbyspinner,getResources().getStringArray(R.array.search_criteria));


        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etKit.getText().length() == 0){
                    etKit.setError("Enter field");
                }
                else{
                    Constants.showLoading(activity);
                    FetchByKitInputTO fetchByKitInputTO = new FetchByKitInputTO();
                    if(searchbyspinner.getSelectedItemPosition() == 0){
                        fetchByKitInputTO.setProxyNumber(etKit.getText().toString());
                    }
                    else if(searchbyspinner.getSelectedItemPosition() == 1){
                        fetchByKitInputTO.setEkycRefNo(etKit.getText().toString());
                    }
                    else if(searchbyspinner.getSelectedItemPosition() == 2){
                        fetchByKitInputTO.setContactNo(etKit.getText().toString());
                    }
                    else if(searchbyspinner.getSelectedItemPosition() == 3){
                        fetchByKitInputTO.setEntityId(etKit.getText().toString());
                    }
                    else if(searchbyspinner.getSelectedItemPosition() == 4){
                        fetchByKitInputTO.setFirstName(etKit.getText().toString());
                    }

                    Constants.getEkycServicesAPI(activity,"adduser").fetchByKit(fetchByKitInputTO, new Callback<AddkycAgentResponseTO>() {
                       @Override
                       public void success(AddkycAgentResponseTO addkycAgentResponseTO, Response response) {
                           Constants.dismissloading();
                           try {
                               addkycAgentResponseTO.getBody().getResults().size();
                               transactionList.clear();
                               for (int i = 0; i < addkycAgentResponseTO.getBody().getResults().size(); i++) {
                                   PendingkycResponse ld = new PendingkycResponse();
                                   ld.setBusiness(addkycAgentResponseTO.getBody().getResults().get(i).getBusiness());
                                   ld.setEntityId(addkycAgentResponseTO.getBody().getResults().get(i).getEntityId());
                                   ld.setId(addkycAgentResponseTO.getBody().getResults().get(i).getId());
                                   ld.setCreated(Constants.getDateCurrentTimeZone(Long.parseLong(addkycAgentResponseTO.getBody().getResults().get(i).getCreated())));
                                   ld.seteKycRefNo(addkycAgentResponseTO.getBody().getResults().get(i).getEkycRefNo());
                                   ld.setKycStatus(addkycAgentResponseTO.getBody().getResults().get(i).getKycStatus());
                                   ld.setKycReason(addkycAgentResponseTO.getBody().getResults().get(i).getKycReason());
                                   ld.setKitNo(addkycAgentResponseTO.getBody().getResults().get(i).getKitNo());
                                   transactionList.add(ld);
                               }
                               customBaseAdapter.notifyDataSetChanged();
                               dialog.dismiss();
                           } catch (Exception e) {
                               Toast.makeText(activity, "Data not found", Toast.LENGTH_SHORT).show();
                               e.printStackTrace();
                           }
                       }

                       @Override
                       public void failure(RetrofitError error) {
                           Constants.dismissloading();
                       }
                   });
                }
            }
        });
        dialog.show();
    }


    public void addFontToSpinner(final Activity activity,final Spinner spinner,final String[] items){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                android.R.layout.simple_spinner_dropdown_item, items) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                Typeface externalFont=Typeface.createFromAsset(activity.getAssets(), getString(R.string.appfont));

                ((TextView) v).setTypeface(externalFont);

                return v;
            }


            public View getDropDownView(int position,  View convertView,  ViewGroup parent) {
                View v =super.getDropDownView(position, convertView, parent);

                Typeface externalFont=Typeface.createFromAsset(activity.getAssets(), getString(R.string.appfont));
                ((TextView) v).setTypeface(externalFont);


                return v;
            }
        };


        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }



}
