package com.anto.kycapp.Fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anto.kycapp.AdduserActivity;
import com.anto.kycapp.BuildConfig;
import com.anto.kycapp.Interface.GetUsersServiceAPI;
import com.anto.kycapp.MainActivity;
import com.anto.kycapp.Models.AddkycAgentResponseTO;
import com.anto.kycapp.Models.AgentLoginResponseTO;
import com.anto.kycapp.Models.CreateorEditKycRequestTo;
import com.anto.kycapp.Models.EditKycResponseTO;
import com.anto.kycapp.Models.GetKycDetailResponseTO;
import com.anto.kycapp.Models.GetKycRequestTo;
import com.anto.kycapp.Models.ResponseTO;
import com.anto.kycapp.Models.UserdataInputTo;
import com.anto.kycapp.R;
import com.anto.kycapp.Utils.Constants;
import com.anto.kycapp.Utils.FontChangeCrawler;
import com.anto.kycapp.Utils.IoUtils;
import com.google.android.gms.location.FusedLocationProviderClient;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.williamww.silkysignature.views.SignaturePad;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class UploadKycFragment extends Fragment {

    LinearLayout signupprooflay1;
    CardView prooflinearlay1,prooflinearlay2,prooflinearlay3,prooflinearlay4,prooflinearlay5,signaturecard;
    ImageView  imageView2, imageView3, imageView4, imageView5, imageView6,customer_imageView,back_img,sig_imageView;
    private  boolean imageloaded,imageloaded1,imageloaded2,imageloaded3,imageloaded4,isupload;
    TextView reasondetailtxt,documenttype1,documenttype2,documenttype3,documenttype4,documenttype5,customerphoto_txt,customersig_txt;
    CheckBox rememberme;
    TextView uploadkyc1,clear,viewdocument1,viewdocument2,viewdocument3,viewdocument4,viewdocument5,viewcustomer,sigtext,viewsig;
    SignaturePad signaturePad;
    Uri picUri,picUri1,picUri2,picUri3,picUri4,picUri5;
    List<MultipartBody.Part> parts = new ArrayList<>();
    String imageFilePath, imageFileName;
    String photoFileName = "kycdocument",sigFileName = "signature", proofphoto = "";
    int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    protected static final int REQUEST_CAMERA = 0;
    CardView reasoncard,gotoekyc;
    int documentslength;
    FusedLocationProviderClient mFusedLocationClient;
    int PERMISSION_ID = 44;
    String customerid,id,business,status,reason,kitno,lattitude,longitude;
    EditText agentname,agentid;
    CreateorEditKycRequestTo createorEditKycRequestTo = new CreateorEditKycRequestTo();
    UserdataInputTo userdataInputTo;

    public UploadKycFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public UploadKycFragment(String status,String customerid, String id, String business,String kitno,String reason){
        this.status = status;
        this.customerid = customerid;
        this.id = id;
        this.business = business;
        this.kitno= kitno;
        this.reason= reason;
    }

    Spinner proofSpinner1,proofSpinner2,proofSpinner3,proofSpinner4,proofSpinner5;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upload_kyc, container, false);
        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), getActivity().getResources().getString(R.string.appfont));
        fontChanger.replaceFonts((ViewGroup)view.findViewById(R.id.uploadkycdoclay));

        Gson gson = new Gson();
        String json = Constants.getSharedPref(getActivity(),"usermodelkey","usermodelclass");
        userdataInputTo = gson.fromJson(json, UserdataInputTo.class);


        reasoncard = (CardView)view.findViewById(R.id.reasoncard);
        gotoekyc = (CardView)view.findViewById(R.id.gotoekyc);


        agentname = view.findViewById(R.id.agentname);
        agentid = view.findViewById(R.id.agentid);

        signupprooflay1 = (LinearLayout)view.findViewById(R.id.signupprooflay1);

        prooflinearlay1 = view.findViewById(R.id.prooflinearlay1);
        prooflinearlay2 = view.findViewById(R.id.prooflinearlay2);
        prooflinearlay3 = view.findViewById(R.id.prooflinearlay3);
        prooflinearlay4 = view.findViewById(R.id.prooflinearlay4);
        prooflinearlay5 = view.findViewById(R.id.prooflinearlay5);
        signaturecard = view.findViewById(R.id.signaturecard);

        reasondetailtxt = (TextView)view.findViewById(R.id.reasondetailtxt);

        documenttype1 = (TextView)view.findViewById(R.id.documenttype1);
        documenttype2 = (TextView)view.findViewById(R.id.documenttype2);
        documenttype3 = (TextView)view.findViewById(R.id.documenttype3);
        documenttype4 = (TextView)view.findViewById(R.id.documenttype4);
        documenttype5 = (TextView)view.findViewById(R.id.documenttype5);
        customerphoto_txt = (TextView)view.findViewById(R.id.customerphoto_txt);
        customersig_txt = (TextView)view.findViewById(R.id.customersig_txt);

        viewdocument1 = (TextView)view.findViewById(R.id.viewdocument1);
        viewdocument2 = (TextView)view.findViewById(R.id.viewdocument2);
        viewdocument3 = (TextView)view.findViewById(R.id.viewdocument3);
        viewdocument4 = (TextView)view.findViewById(R.id.viewdocument4);
        viewdocument5 = (TextView)view.findViewById(R.id.viewdocument5);
        viewcustomer = (TextView)view.findViewById(R.id.viewcustomer);
        sigtext = (TextView)view.findViewById(R.id.sigtext);
        viewsig = (TextView)view.findViewById(R.id.viewsig);


        uploadkyc1 = (TextView)view.findViewById(R.id.signup1);
        back_img = (ImageView) view.findViewById(R.id.back_img);

        imageView2 = (ImageView) view.findViewById(R.id.upload_imageView2);
        imageView3 = (ImageView) view.findViewById(R.id.upload_imageView3);
        imageView4 = (ImageView) view.findViewById(R.id.upload_imageView4);
        imageView5 = (ImageView) view.findViewById(R.id.upload_imageView5);
        imageView6 = (ImageView) view.findViewById(R.id.upload_imageView6);
        customer_imageView = view.findViewById(R.id.customer_imageView);
        sig_imageView = view.findViewById(R.id.sig_imageView);

        signaturePad = (SignaturePad)view.findViewById(R.id.signature_pad);
        clear = (TextView)view.findViewById(R.id.clear);

        proofSpinner1 = (Spinner)view.findViewById(R.id.upload_proofSpinner1);
        proofSpinner2 = (Spinner)view.findViewById(R.id.upload_proofSpinner2);
        proofSpinner3 = (Spinner)view.findViewById(R.id.upload_proofSpinner3);
        proofSpinner4 = (Spinner)view.findViewById(R.id.upload_proofSpinner4);
        proofSpinner5 = (Spinner)view.findViewById(R.id.upload_proofSpinner5);

        rememberme = view.findViewById(R.id.rememberme);

        mFusedLocationClient = new FusedLocationProviderClient(getActivity());

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signaturePad.clear();
            }
        });

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        if(Constants.getSharedPref(getActivity(),Constants.REM_PREFS,"remagentname") != null){
           agentname.setText(Constants.getSharedPref(getActivity(),Constants.REM_PREFS,"remagentname"));
           agentid.setText(Constants.getSharedPref(getActivity(),Constants.REM_PREFS,"remagentid"));
        }

        if(agentname.getText().length() != 0){
            rememberme.setChecked(true);
        }

        rememberme.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(compoundButton.isChecked()){
                    if (agentname.getText().length() != 0 && agentid.getText().length() != 0) {
                        Constants.setSharedPref(getActivity(),Constants.REM_PREFS,"remagentname",agentname.getText().toString());
                        Constants.setSharedPref(getActivity(),Constants.REM_PREFS,"remagentid",agentid.getText().toString());
                    }
                    else {
                        Toast.makeText(getActivity(), "Enter agentname and agentid", Toast.LENGTH_LONG).show();
                        compoundButton.setChecked(false);
                    }
                }
                else{
                    Constants.deleteSessionPreferences(getContext());
                }
            }
        });

        uploadkyc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    uploadKyc();
                }
            }
        });


        if (status.equals("FAILED")) {
            if(reason!=null){
                reasoncard.setVisibility(View.VISIBLE);
                reasondetailtxt.setText(reason);
            }
        }



        gotoekyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),AdduserActivity.class).putExtra("Kitno",kitno));
            }
        });


        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isupload) {
                    proofphoto = "proof1";
                    getUploadpicOption();
                }
            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isupload) {
                    proofphoto = "proof2";
                    getUploadpicOption();
                }
            }
        });

        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isupload) {
                    proofphoto = "proof3";
                    getUploadpicOption();
                }
            }
        });
        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isupload) {
                    proofphoto = "proof4";
                    getUploadpicOption();
                }
            }
        });
        imageView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isupload) {
                    proofphoto = "proof5";
                    getUploadpicOption();
                }
            }
        });
        customer_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("isupload++"+isupload);
                if (!isupload) {
                    proofphoto = "customerphoto";
                    getUploadpicOption();
                }
            }
        });

        viewcustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage(getActivity(),customer_imageView);
            }
        });

        viewdocument1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage(getActivity(),imageView2);
            }
        });

        viewdocument2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage(getActivity(),imageView3);
            }
        });

        viewdocument3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage(getActivity(),imageView4);
            }
        });

        viewdocument4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage(getActivity(),imageView5);
            }
        });

        viewdocument5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage(getActivity(),imageView6);
            }
        });

        viewsig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage(getActivity(),sig_imageView);
            }
        });

        getLastLocation();
        getKycDetails();
        return view;
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                MultipartBody.FORM, descriptionString);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
        File file;
        file = getFilePathFromURI(getActivity(),fileUri);
        System.out.println("file path in upload kyc"+getActivity().getContentResolver().getType(fileUri));

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(getActivity().getContentResolver().getType(fileUri)),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    public  File getFilePathFromURI(Context context, Uri contentUri) {
        //copy file and send new file path
        String fileName = getFileName(contentUri);
        if (!TextUtils.isEmpty(fileName)) {
            File mediaStorageDir = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES), MainActivity.APP_TAG);
            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
//                Log.d(APP_TAG, "failed to create directory");
            }
            File copyFile = new File(mediaStorageDir.getPath() + File.separator + fileName);
            copy(context, contentUri, copyFile);
            return copyFile;
        }
        return null;
    }

    public static String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IoUtils.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getKycDetails(){
        Constants.showLoading(getActivity());
        final ArrayList<TextView> spinners = new ArrayList<>();
        spinners.add(documenttype1);
        spinners.add(documenttype2);
        spinners.add(documenttype3);
        spinners.add(documenttype4);
        spinners.add(documenttype5);

        final ArrayList<ImageView> imageViews = new ArrayList<>();
        imageViews.add(imageView2);
        imageViews.add(imageView3);
        imageViews.add(imageView4);
        imageViews.add(imageView5);
        imageViews.add(imageView6);


        final ArrayList<CardView> prooflinearlays = new ArrayList<>();
        prooflinearlays.add(prooflinearlay1);
        prooflinearlays.add(prooflinearlay2);
        prooflinearlays.add(prooflinearlay3);
        prooflinearlays.add(prooflinearlay4);
        prooflinearlays.add(prooflinearlay5);

        GetKycRequestTo getKycRequestTo = new GetKycRequestTo();
        getKycRequestTo.setBusiness(business);
        getKycRequestTo.setKycRefNo(kitno);
        getKycRequestTo.setBank("M2PBANK");

        Constants.getEkycServicesAPI(getActivity(),business).getKycDetails(getKycRequestTo, new Callback<GetKycDetailResponseTO>() {
            @Override
            public void success(GetKycDetailResponseTO getKycDetailResponseTO, retrofit.client.Response response) {
                Constants.dismissloading();
                documentslength = getKycDetailResponseTO.getResult().getDocuments().size();

                for (int i=0; i < getKycDetailResponseTO.getResult().getDocuments().size();i++){
                    if (getKycDetailResponseTO.getResult().getDocuments().get(i).getDocumentType()!=null) {
                        if (getKycDetailResponseTO.getResult().getDocuments().get(i).getDocumentType().equals("customerImage")) {
                            viewcustomer.setVisibility(View.VISIBLE);
                            setImageBitmapBase64(getKycDetailResponseTO.getResult().getDocuments().get(i).getBase64String(), customer_imageView);
                            customerphoto_txt.setText(getKycDetailResponseTO.getResult().getDocuments().get(i).getDocumentType());
                            if (status.equals("PENDING")) {
                                sigtext.setText(getActivity().getString(R.string.approval_str));
                                signaturePad.setVisibility(View.GONE);
                                uploadkyc1.setVisibility(View.GONE);
                                clear.setVisibility(View.GONE);
                                isupload = true;
                            }
                        } else if (getKycDetailResponseTO.getResult().getDocuments().get(i).getDocumentType().equals("signatureImage")) {
                            signaturecard.setVisibility(View.VISIBLE);
                            customersig_txt.setText(getKycDetailResponseTO.getResult().getDocuments().get(i).getDocumentType());
                            setImageBitmapBase64(getKycDetailResponseTO.getResult().getDocuments().get(i).getBase64String(), sig_imageView);
                        } else {
                            prooflinearlays.get(i).setVisibility(View.VISIBLE);
                            spinners.get(i).setText(getKycDetailResponseTO.getResult().getDocuments().get(i).getDocumentType());

                            if (i == 0) {
                                imageloaded = true;
                            } else if (i == 1) {
                                imageloaded1 = true;
                            } else if (i == 2) {
                                imageloaded2 = true;
                            } else if (i == 3) {
                                imageloaded3 = true;
                            } else if (i == 4) {
                                imageloaded4 = true;
                            }

                            setImageBitmapBase64(getKycDetailResponseTO.getResult().getDocuments().get(i).getBase64String(), imageViews.get(i));
                        }
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Constants.dismissloading();

            }
        });
    }
    public void setImageBitmapBase64(String base64,ImageView imageview){
        imageview.setBackgroundResource(0);
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length,options);
        imageview.setImageBitmap(decodedByte);
    }

    private int getIndex(Spinner spinner, String myString){
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                return i;
            }else if (spinner.getItemAtPosition(i).toString().equals(myString)){
                return i;
            }
        }
        return 0;
    }

    public void getUploadpicOption() {

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            Intent pictureIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            if (pictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                //Create a file to store the image
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File
                }

                if (photoFile != null) {
                    picUri = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                    if (proofphoto.equals("proof1")) {
                        //picUri = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri);
                    } else if (proofphoto.equals("proof2")) {
                        //picUri1 = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri);
                    }else if (proofphoto.equals("proof3")) {
                        //picUri2 = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri);
                    }else if (proofphoto.equals("proof4")) {
                       // picUri3 = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri);
                    }else if (proofphoto.equals("proof5")) {
                        //picUri4 = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri);
                    }
                    else {
                        //picUri5 = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri);
                    }
                    startActivityForResult(pictureIntent,
                            REQUEST_CAMERA);
                }
            }

        } else {
//                      Toast.makeText(getActivity(), "You dont have permission to access Camera!", Toast.LENGTH_LONG).show();
            getCameraPermission();
        }

    }


    public static boolean reduceImage(String path, long maxSize) {
        File img = new File(path);
        boolean result = false;
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bitmap = null;
        options.inSampleSize=1;
        while (img.length()>maxSize) {
            options.inSampleSize = options.inSampleSize+1;
            bitmap = BitmapFactory.decodeFile(path, options);
            img.delete();
            try
            {
                FileOutputStream fos = new FileOutputStream(path);
                bitmap.compress(path.toLowerCase().endsWith("jpg")?
                        Bitmap.CompressFormat.JPEG:
                        Bitmap.CompressFormat.JPEG, 100, fos);
                fos.close();
                result = true;
            }catch (Exception errVar) {
                errVar.printStackTrace();
            }
        }
        return result;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        imageFileName = sigFileName + timeStamp + "_";
        File storageDir =
                getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

            FileOutputStream fOut = new FileOutputStream(image);

            inImage.compress(Bitmap.CompressFormat.PNG, 50, fOut);
            fOut.flush();
            fOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", image);
    }

    private File createImageFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        imageFileName = photoFileName + timeStamp + "_";
        File storageDir =
                getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();

        return image;
    }

    private void getCameraPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

            //System.out.println("picuriiiiiii uploadkyc" + picUri.getPath());

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
            options.inJustDecodeBounds = false;
            Bitmap bitmap = BitmapFactory.decodeFile(imageFilePath,options);




            switch (requestCode) {

                case REQUEST_CAMERA:
                    if (resultCode == RESULT_OK) {
                        picUri = Constants.overwriteBitmap(getActivity(),getFileName(picUri),bitmap,Bitmap.CompressFormat.JPEG,80,"Resized");
                        if (proofphoto.equals("proof1")) {
                            if (Constants.hasImage(imageView2)) {
                                if (!imageloaded) {
                                    parts.add(prepareFilePart(documenttype1.getText().toString(),picUri));
                                    parts.remove(parts.size()-2);
                                    imageView2.setImageBitmap(bitmap);
                                } else {
                                    imageloaded = false;
                                    imageView2.setImageBitmap(bitmap);
                                    parts.add(prepareFilePart(documenttype1.getText().toString(),picUri));
                                }
                            } else {
                                imageView2.setImageBitmap(bitmap);
                                parts.add(prepareFilePart(documenttype1.getText().toString(),picUri));
                            }
                        } else if (proofphoto.equals("proof2")) {
                            if (Constants.hasImage(imageView3)) {
                                if (!imageloaded1) {
                                    imageView3.setImageBitmap(bitmap);
                                    parts.add(prepareFilePart(documenttype2.getText().toString(),picUri));
                                    parts.remove(parts.size()-2);
                                } else {
                                    imageView3.setImageBitmap(bitmap);
                                    parts.add(prepareFilePart(documenttype2.getText().toString(),picUri));
                                }
                            } else {
                                imageView3.setImageBitmap(bitmap);
                                parts.add(prepareFilePart(documenttype2.getText().toString(),picUri));
                            }
                        } else if (proofphoto.equals("proof3")) {
                            if (Constants.hasImage(imageView4)) {
                                if (!imageloaded2) {
                                    imageView4.setImageBitmap(bitmap);
                                    parts.add(prepareFilePart(documenttype3.getText().toString(),picUri));
                                    parts.remove(parts.size()-2);
                                } else {
                                    imageView4.setImageBitmap(bitmap);
                                    parts.add(prepareFilePart(documenttype3.getText().toString(),picUri));
                                }
                            } else {
                                imageView4.setImageBitmap(bitmap);
                                parts.add(prepareFilePart(documenttype3.getText().toString(),picUri));
                            }
                        } else if (proofphoto.equals("proof4")) {
                            if (Constants.hasImage(imageView5)) {
                                if (!imageloaded3) {
                                    imageView5.setImageBitmap(bitmap);
                                    parts.add(prepareFilePart(documenttype4.getText().toString(),picUri));
                                    parts.remove(parts.size()-2);
                                } else {
                                    imageView5.setImageBitmap(bitmap);
                                    parts.add(prepareFilePart(documenttype4.getText().toString(),picUri));
                                }
                            } else {
                                imageView5.setImageBitmap(bitmap);
                                parts.add(prepareFilePart(documenttype4.getText().toString(),picUri));
                            }
                        } else if (proofphoto.equals("proof5")) {
                            if (Constants.hasImage(imageView6)) {
                                if (!imageloaded4) {
                                    imageView6.setImageBitmap(bitmap);
                                    parts.add(prepareFilePart(documenttype5.getText().toString(),picUri));
                                    parts.remove(parts.size()-2);
                                } else {
                                    imageView6.setImageBitmap(bitmap);
                                    parts.add(prepareFilePart(documenttype5.getText().toString(),picUri));
                                }
                            } else {
                                imageView6.setImageBitmap(bitmap);
                                parts.add(prepareFilePart(documenttype5.getText().toString(),picUri));
                            }
                        }
                        else{
                            if (Constants.hasImage(customer_imageView)) {
                                customer_imageView.setImageBitmap(bitmap);
                                parts.add(prepareFilePart("customerImage",picUri));
                                parts.remove(parts.size()-2);
                            } else {
                                viewcustomer.setVisibility(View.VISIBLE);
                                customer_imageView.setImageBitmap(bitmap);
                                parts.add(prepareFilePart("customerImage",picUri));
                            }
                        }
                    }

        }
    }

    public boolean validate(){
        if(agentname.getText().length() == 0){
            Constants.showCustomDialog(getActivity(),"Enter AgentName..");
            return false;
        }
        if(agentid.getText().length() == 0){
            Constants.showCustomDialog(getActivity(),"Enter AgentID..");
            return false;
        }
        if(lattitude == null || longitude == null){
            Constants.showCustomDialog(getActivity(),"Location not found ,try again..");
            getLastLocation();
            return false;
        }
        if(!Constants.hasImage(customer_imageView)){
            Constants.showCustomDialog(getActivity(),"Get customer photo");
            return false;
        }
        if(signaturePad.isEmpty()){
            Constants.showCustomDialog(getActivity(),"Get customer signature");
            return false;
        }
        return true;
    }

    public void uploadKyc(){
        Constants.showLoading(getActivity());
        Uri siguri = getImageUri(getActivity(),signaturePad.getSignatureBitmap());
        siguri = Constants.overwriteBitmap(getActivity(),getFileName(siguri),signaturePad.getSignatureBitmap(),Bitmap.CompressFormat.JPEG,80,"Resized");

        parts.add(prepareFilePart("signatureImage",siguri));
        createorEditKycRequestTo.setBankName("M2PBANK");
        createorEditKycRequestTo.setCustomerId(customerid);
        createorEditKycRequestTo.setBusiness(business);
        createorEditKycRequestTo.setEkycRefNo(kitno);
        createorEditKycRequestTo.setDocuments(documentslength);
        createorEditKycRequestTo.setAgentName(agentname.getText().toString());
        createorEditKycRequestTo.setAgentId(agentid.getText().toString());
        createorEditKycRequestTo.setLocation(lattitude+" "+longitude);

        final String json = new Gson().toJson(createorEditKycRequestTo);
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        //loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                String token = "Basic "+Constants.serviceUrls.SESSIONTOKEN;
                //System.out.println("requestInterceptor upload kycmain"+token);

                Request original = chain.request();

                Request.Builder builder = original.newBuilder();
                builder.header("Content-Type","multipart/form-data");
                builder.header("Authorization",token);

                Request request = builder.method(original.method(), original.body())
                        .build();
                Log.e("header in uploadkyc",request.header("Authorization"));
                Log.e("header in uploadkyc",request.header("Content-Type"));
                return chain.proceed(request);
            }
        });
        httpClient.addInterceptor(loggingInterceptor);

        Retrofit client = new Retrofit.Builder()
                .baseUrl(BuildConfig.KycBaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        RequestBody jsonstring = createPartFromString(json);
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("jsonBody",jsonstring);
        System.out.println("Content uploadkyc++++"+status+jsonstring.contentType()+json);

        GetUsersServiceAPI getserviceApi = client.create(GetUsersServiceAPI.class);

        if (status.equals("UPLOAD_KYC")) {
            getserviceApi.createKyc(map,parts).enqueue(new retrofit2.Callback<AgentLoginResponseTO>() {
                @Override
                public void onResponse(Call<AgentLoginResponseTO> call, Response<AgentLoginResponseTO> response) {
                    if(response.isSuccessful()){
                        Constants.dismissloading();
                        Constants.showUploadeddialog(getActivity());
                    }
                    else{
                        Constants.dismissloading();
                        try {
                            Gson gson = new Gson();
                            ResponseTO responseTO = gson.fromJson(response.errorBody().charStream(), ResponseTO.class);
                            Toast.makeText(getActivity(), responseTO.getException().getDetailMessage(), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "Proxy server error", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<AgentLoginResponseTO> call, Throwable t) {
                    Constants.dismissloading();
                    System.out.println("Error in upload"+t.getLocalizedMessage());
                    Toast.makeText(getActivity(), "Error in uploadKYC"+t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            getserviceApi.editKyc(map,parts).enqueue(new retrofit2.Callback<EditKycResponseTO>() {
                @Override
                public void onResponse(Call<EditKycResponseTO> call, Response<EditKycResponseTO> response) {
                    if(response.isSuccessful()){
                        Constants.dismissloading();
                        Constants.showUploadeddialog(getActivity());
                    }
                    else{
                        Constants.dismissloading();
                        try {
                            Gson gson = new Gson();
                            ResponseTO responseTO = gson.fromJson(response.errorBody().charStream(), ResponseTO.class);
                            Toast.makeText(getActivity(), responseTO.getException().getDetailMessage(), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "Proxy server error", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EditKycResponseTO> call, Throwable t) {
                    Constants.dismissloading();
                    System.out.println("Error in upload"+t.getLocalizedMessage());
                    Toast.makeText(getActivity(), "Error in uploadKYC"+t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }



    @SuppressLint("MissingPermission")
    private void getLastLocation(){
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    lattitude = String.valueOf(location.getLatitude());
                                    longitude = String.valueOf(location.getLongitude());
                                }
                            }
                        }
                );
            } else {
                Toast.makeText(getActivity(), "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }


    @SuppressLint("MissingPermission")
    private void requestNewLocationData(){

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);


        mFusedLocationClient = new FusedLocationProviderClient(getActivity());
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            lattitude = String.valueOf(mLastLocation.getLatitude());
            longitude = String.valueOf(mLastLocation.getLongitude());
        }
    };

    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                getActivity(),
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            }
        }
    }

    public void showImage(final Activity activity, ImageView imageUri){
        final Dialog dialog = new Dialog(activity,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.viewimage_dialog);

        RelativeLayout close = dialog.findViewById(R.id.close);
        ImageView imageView = dialog.findViewById(R.id.image_doc);

        Bitmap image = ((BitmapDrawable)imageUri.getDrawable()).getBitmap();
        imageView.setImageBitmap(image);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
