package com.anto.kycapp.Fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anto.kycapp.AdduserActivity;
import com.anto.kycapp.BuildConfig;
import com.anto.kycapp.Interface.GetUsersServiceAPI;
import com.anto.kycapp.MainActivity;
import com.anto.kycapp.Models.AdduserrequestTO;
import com.anto.kycapp.Models.AgentLoginResponseTO;
import com.anto.kycapp.Models.ResponseTO;
import com.anto.kycapp.Models.UserDetailsResponseTo;
import com.anto.kycapp.Models.UserdataInputTo;
import com.anto.kycapp.R;
import com.anto.kycapp.Utils.Constants;
import com.anto.kycapp.Utils.DatePickerDialogFragment;
import com.anto.kycapp.Utils.FileUtils;
import com.anto.kycapp.Utils.FontChangeCrawler;
import com.anto.kycapp.Utils.IoUtils;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.app.Activity.RESULT_OK;
import static com.anto.kycapp.MainActivity.APP_TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class UserRegisterDetailFragment extends Fragment {
    ImageView imageView2,imageView3,imageView4,imageView5,imageView6;
    boolean imgclick1,imgclick2,imgclick3,imgclick4,imgclick5;
    UserdataInputTo userdataInputTo;
    List<MultipartBody.Part> parts = new ArrayList<>();
    int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    public static final int EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 2;
    public static final int EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_READ = 3;
    protected static final int REQUEST_CAMERA = 0;
    protected static final int PICK_IMAGE_REQUEST = 1;
    public String photoFileName = "kycdocument",proofphoto="",imageFileName,imageFilePath,userid,corporate;
    private Uri picUri,picUri1,picUri2,picUri3,picUri4;
    CardView customerreg1,customerreg2,customerreg3,customerreg4,reasoncard,gotoekyc;
    TextView next_txt1,next_txt2,next_txt3,text_proof1,text_proof2,text_proof3,text_proof4,text_proof5;
    TextView signup1,signup2,signup3,signup4,signup5,reasondetailtxt;
    LinearLayout prooflinearlay1,prooflinearlay2,prooflinearlay3,prooflinearlay4,prooflinearlay5;
    LinearLayout signupprooflay1,signupprooflay2,signupprooflay3,signupprooflay4,signupprooflay5;
    public AdduserrequestTO adduserrequestTO = new AdduserrequestTO();
    MaterialEditText et_firstname,et_lastname,et_mobileNumber,et_customerId,et_kitnumber,et_mailId,et_addressline,et_addressline1,et_city,et_pincode,et_idnumber;
    TextView txt_dateofbirth,idexpiry,back_txt1,back_txt2,back_txt3;
    Spinner titleSpinner,genderSpinner,cardtypeSpinner,corporateSpinner,stateSpinner,proofSpinner,kyctypeSpinner,proofSpinner1,proofSpinner2,proofSpinner3,proofSpinner4,proofSpinner5;
    boolean usercheckonce = false;


    public UserRegisterDetailFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public UserRegisterDetailFragment(String userid, String corporate){
        this.userid = userid;
        this.corporate = corporate;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_user_register_detail, container, false);
        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), this.getResources().getString(R.string.appfont));
        fontChanger.replaceFonts((ViewGroup)view.findViewById(R.id.reguserdetaillay));

        Gson gson = new Gson();
        String json = Constants.getSharedPref(getActivity(),"usermodelkey","usermodelclass");
        userdataInputTo = gson.fromJson(json, UserdataInputTo.class);

        Constants.serviceUrls.SESSIONTOKEN = Constants.getSharedPref(getActivity(),"sessiontoken","securitytoken");
        //System.out.println("THe token is"+Constants.serviceUrls.SESSIONTOKEN);


        imageView2 = (ImageView)view.findViewById(R.id.imageView2);
        imageView3 = (ImageView)view.findViewById(R.id.imageView3);
        imageView4 = (ImageView)view.findViewById(R.id.imageView4);
        imageView5 = (ImageView)view.findViewById(R.id.imageView5);
        imageView6 = (ImageView)view.findViewById(R.id.imageView6);

        et_firstname = (MaterialEditText)view.findViewById(R.id.firstname);
        et_lastname = (MaterialEditText)view.findViewById(R.id.lastname);
        et_mobileNumber = (MaterialEditText)view.findViewById(R.id.mobileNumber);
        et_customerId = (MaterialEditText)view.findViewById(R.id.customerId);
        et_kitnumber = (MaterialEditText)view.findViewById(R.id.kitNumber);
        et_mailId = (MaterialEditText)view.findViewById(R.id.emailId);

        et_addressline = (MaterialEditText)view.findViewById(R.id.et_addressline);
        et_addressline1 = (MaterialEditText)view.findViewById(R.id.et_addressline1);
        et_city = (MaterialEditText)view.findViewById(R.id.et_city);
        et_pincode = (MaterialEditText)view.findViewById(R.id.et_pincode);
        et_idnumber = (MaterialEditText)view.findViewById(R.id.et_idnumber);

        txt_dateofbirth = (TextView)view.findViewById(R.id.txt_dob);
        idexpiry = (TextView)view.findViewById(R.id.txt_idexpiry);

        titleSpinner = (Spinner)view.findViewById(R.id.titleSpinner);
        genderSpinner = (Spinner)view.findViewById(R.id.genderSpinner);
        cardtypeSpinner = (Spinner)view.findViewById(R.id.cardtypeSpinner);
        corporateSpinner = (Spinner)view.findViewById(R.id.corporateSpinner);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, android.R.id.text1);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        corporateSpinner.setAdapter(spinnerAdapter);
        spinnerAdapter.add("Select corporate");
        spinnerAdapter.add(userdataInputTo.getBusiness());
        spinnerAdapter.notifyDataSetChanged();

        stateSpinner = (Spinner)view.findViewById(R.id.stateSpinner);
        proofSpinner = (Spinner)view.findViewById(R.id.proofSpinner);
        kyctypeSpinner = (Spinner)view.findViewById(R.id.kyctypeSpinner);

        proofSpinner1 = (Spinner)view.findViewById(R.id.proofSpinner1);
        proofSpinner2 = (Spinner)view.findViewById(R.id.proofSpinner2);
        proofSpinner3 = (Spinner)view.findViewById(R.id.proofSpinner3);
        proofSpinner4 = (Spinner)view.findViewById(R.id.proofSpinner4);
        proofSpinner5 = (Spinner)view.findViewById(R.id.proofSpinner5);


        proofSpinner1.setSelection(0,false);
        proofSpinner2.setSelection(0,false);
        proofSpinner3.setSelection(0,false);
        proofSpinner4.setSelection(0,false);
        proofSpinner5.setSelection(0,false);

        proofSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( proofSpinner2.getSelectedItemPosition()== position || proofSpinner3.getSelectedItemPosition()== position || proofSpinner4.getSelectedItemPosition()== position || proofSpinner5.getSelectedItemPosition()== position){
                    proofSpinner1.setSelection(0);
                    Toast.makeText(getActivity(), R.string.proofalreadyselect, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        proofSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (proofSpinner1.getSelectedItemPosition()== position || proofSpinner3.getSelectedItemPosition()== position || proofSpinner4.getSelectedItemPosition()== position || proofSpinner5.getSelectedItemPosition()== position){
                    proofSpinner2.setSelection(0);
                    Toast.makeText(getActivity(), R.string.proofalreadyselect, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        proofSpinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (proofSpinner1.getSelectedItemPosition()== position || proofSpinner2.getSelectedItemPosition()== position  || proofSpinner4.getSelectedItemPosition()== position || proofSpinner5.getSelectedItemPosition()== position){
                    proofSpinner3.setSelection(0);
                    Toast.makeText(getActivity(), R.string.proofalreadyselect, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        proofSpinner4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (proofSpinner1.getSelectedItemPosition()== position || proofSpinner2.getSelectedItemPosition()== position || proofSpinner3.getSelectedItemPosition()== position || proofSpinner5.getSelectedItemPosition()== position){
                    proofSpinner4.setSelection(0);
                    Toast.makeText(getActivity(), R.string.proofalreadyselect, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        proofSpinner5.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (proofSpinner1.getSelectedItemPosition()== position || proofSpinner2.getSelectedItemPosition()== position || proofSpinner3.getSelectedItemPosition()== position || proofSpinner4.getSelectedItemPosition()== position){
                    proofSpinner5.setSelection(0);
                    Toast.makeText(getActivity(), R.string.proofalreadyselect, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        reasoncard = (CardView)view.findViewById(R.id.reasoncard);
        gotoekyc = (CardView)view.findViewById(R.id.gotoekyc);

        gotoekyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AdduserActivity.class).putExtra("Kitno",et_kitnumber.getText().toString()));
            }
        });

        customerreg1 = (CardView)view.findViewById(R.id.customerreg1);
        customerreg2 = (CardView)view.findViewById(R.id.customerreg2);
        customerreg3 = (CardView)view.findViewById(R.id.customerreg3);
        customerreg4 = (CardView)view.findViewById(R.id.customerreg4);

        reasondetailtxt = (TextView)view.findViewById(R.id.reasondetailtxt);

        next_txt1 = (TextView)view.findViewById(R.id.next_txt1);
        next_txt2 = (TextView)view.findViewById(R.id.next_txt2);
        next_txt3 = (TextView)view.findViewById(R.id.next_txt3);

        back_txt1 = (TextView)view.findViewById(R.id.back_txt1);
        back_txt2 = (TextView)view.findViewById(R.id.back_txt2);
        back_txt3 = (TextView)view.findViewById(R.id.back_txt3);

        text_proof1 = (TextView)view.findViewById(R.id.text_proof1);
        text_proof2 = (TextView)view.findViewById(R.id.text_proof2);
        text_proof3 = (TextView)view.findViewById(R.id.text_proof3);
        text_proof4 = (TextView)view.findViewById(R.id.text_proof4);
        text_proof5 = (TextView)view.findViewById(R.id.text_proof5);

        signup1 = (TextView)view.findViewById(R.id.signup1);
        signup2 = (TextView)view.findViewById(R.id.signup2);
        signup3 = (TextView)view.findViewById(R.id.signup3);
        signup4 = (TextView)view.findViewById(R.id.signup4);
        signup5 = (TextView)view.findViewById(R.id.signup5);

        prooflinearlay1 = (LinearLayout) view.findViewById(R.id.prooflinearlay1);
        prooflinearlay2 = (LinearLayout) view.findViewById(R.id.prooflinearlay2);
        prooflinearlay3 = (LinearLayout) view.findViewById(R.id.prooflinearlay3);
        prooflinearlay4 = (LinearLayout) view.findViewById(R.id.prooflinearlay4);
        prooflinearlay5 = (LinearLayout) view.findViewById(R.id.prooflinearlay5);

        signupprooflay1 = (LinearLayout) view.findViewById(R.id.signupprooflay1);
        signupprooflay2 = (LinearLayout) view.findViewById(R.id.signupprooflay2);
        signupprooflay3 = (LinearLayout) view.findViewById(R.id.signupprooflay3);
        signupprooflay4 = (LinearLayout) view.findViewById(R.id.signupprooflay4);
        signupprooflay5 = (LinearLayout) view.findViewById(R.id.signupprooflay5);

        txt_dateofbirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Date date;

                    Calendar c = Calendar.getInstance();
                    c.add(Calendar.YEAR,-18);
                    if (txt_dateofbirth.getText().toString().length()==0) {
                        String currentDateTimeString = Constants.dateFormat.format(c.getTime());
                        date = Constants.dateFormat.parse(currentDateTimeString);
                    } else {
                        date = Constants.dateFormat.parse(txt_dateofbirth.getText().toString());
                    }
                    c.setTime(date);
                    DialogFragment picker = new DatePickerDialogFragment(txt_dateofbirth, c,"dob");
                    picker.show(getActivity().getSupportFragmentManager(),"Date of birth");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        idexpiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Date date;

                    Calendar c = Calendar.getInstance();
                    //c.add(Calendar.YEAR,);
                    if (idexpiry.getText().toString().length()==0) {
                        String currentDateTimeString = Constants.dateFormat.format(c.getTime());
                        date = Constants.dateFormat.parse(currentDateTimeString);
                    } else {
                        date = Constants.dateFormat.parse(idexpiry.getText().toString());
                    }
                    c.setTime(date);
                    DialogFragment picker = new DatePickerDialogFragment(idexpiry, c,"idexpiry");
                    picker.show(getActivity().getSupportFragmentManager(),"ID Expiry");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        next_txt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validatecustomerdetail1()) {
                    adduserrequestTO.setId(userid);
                    adduserrequestTO.setStatus(Integer.parseInt(userdataInputTo.getStatus()));
                    adduserrequestTO.setAuthLevel(Integer.parseInt(userdataInputTo.getAuthLevel()));
                    adduserrequestTO.setUserLevel("");
                    adduserrequestTO.setBusiness(userdataInputTo.getBusiness());
                    adduserrequestTO.setFirst_Name(et_firstname.getText().toString());
                    adduserrequestTO.setLast_Name(et_lastname.getText().toString());
                    adduserrequestTO.setContact_No(et_mobileNumber.getText().toString());
                    adduserrequestTO.setCustomerId(et_customerId.getText().toString());
                    adduserrequestTO.setKit_No(et_kitnumber.getText().toString());
                    adduserrequestTO.setEmail_Address(et_mailId.getText().toString());
                    adduserrequestTO.setDate_Of_Birth(txt_dateofbirth.getText().toString());
                    adduserrequestTO.setTitle(titleSpinner.getSelectedItem().toString());
                    if (genderSpinner.getSelectedItem().toString().equals("Male")) {
                        adduserrequestTO.setGender("M");
                    } else {
                        adduserrequestTO.setGender("F");
                    }
                    if (cardtypeSpinner.getSelectedItem().toString().equals("Physical")) {
                        adduserrequestTO.setCard_Type("P");
                    } else {
                        adduserrequestTO.setCard_Type("V");
                    }
                    adduserrequestTO.setCorporate_Id(corporateSpinner.getSelectedItem().toString());
                    customerreg1.setVisibility(View.GONE);
                    reasoncard.setVisibility(View.GONE);
                    customerreg2.setVisibility(View.VISIBLE);
                }
            }
        });

        next_txt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validatecustomerdetail2()) {
                    adduserrequestTO.setState(stateSpinner.getSelectedItem().toString());
                    adduserrequestTO.setAddress_Line1(et_addressline.getText().toString());
                    adduserrequestTO.setAddress_Line2(et_addressline1.getText().toString());
                    adduserrequestTO.setCity(et_city.getText().toString());
                    adduserrequestTO.setPincode(et_pincode.getText().toString());
                    adduserrequestTO.setCountry("India");
                    customerreg2.setVisibility(View.GONE);
                    customerreg3.setVisibility(View.VISIBLE);
                }
            }
        });

        next_txt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validatecustomerdetail3()) {
                    if (proofSpinner.getSelectedItemPosition()== 4) {
                        adduserrequestTO.setId_Type("DL");
                    } else {
                        adduserrequestTO.setId_Type(proofSpinner.getSelectedItem().toString());
                    }
                    adduserrequestTO.setId_Number(et_idnumber.getText().toString());
                    adduserrequestTO.setExpiry_Date(idexpiry.getText().toString());
                    adduserrequestTO.setKYC_Status(kyctypeSpinner.getSelectedItem().toString());
                    adduserrequestTO.setUserId("santhos@m2p.in");

                    customerreg3.setVisibility(View.GONE);
                    customerreg4.setVisibility(View.VISIBLE);
                }
            }
        });

        back_txt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerreg2.setVisibility(View.GONE);
                customerreg1.setVisibility(View.VISIBLE);
                reasoncard.setVisibility(View.VISIBLE);
            }
        });

        back_txt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerreg3.setVisibility(View.GONE);
                customerreg2.setVisibility(View.VISIBLE);
            }
        });

        back_txt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerreg4.setVisibility(View.GONE);
                customerreg3.setVisibility(View.VISIBLE);
            }
        });

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgclick1=true;
                proofphoto="proof1";
                getUploadpicOption();
            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgclick2=true;
                proofphoto="proof2";
                getUploadpicOption();
            }
        });

        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgclick3=true;
                proofphoto="proof3";
                getUploadpicOption();
            }
        });
        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgclick4=true;
                proofphoto="proof4";
                getUploadpicOption();
            }
        });
        imageView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgclick5=true;
                proofphoto="proof5";
                getUploadpicOption();
            }
        });


        signup1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usercheckonce) {
                    updateUser();
                } else {
                    showUploadeddialog(getActivity());
                }
            }
        });

        signup2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usercheckonce) {
                    updateUser();
                } else {
                    showUploadeddialog(getActivity());
                }
            }
        });

        signup3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usercheckonce) {
                    updateUser();
                } else {
                    showUploadeddialog(getActivity());
                }
            }
        });

        signup4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usercheckonce) {
                    updateUser();
                } else {
                    showUploadeddialog(getActivity());
                }
            }
        });

        signup5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usercheckonce) {
                    updateUser();
                } else {
                    showUploadeddialog(getActivity());
                }
            }
        });

        text_proof1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupprooflay1.setVisibility(View.GONE);
                prooflinearlay2.setVisibility(View.VISIBLE);
            }
        });

        text_proof2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupprooflay2.setVisibility(View.GONE);
                prooflinearlay3.setVisibility(View.VISIBLE);
            }
        });

        text_proof3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupprooflay3.setVisibility(View.GONE);
                prooflinearlay4.setVisibility(View.VISIBLE);
            }
        });

        text_proof4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupprooflay4.setVisibility(View.GONE);
                text_proof5.setVisibility(View.GONE);
                prooflinearlay5.setVisibility(View.VISIBLE);
            }
        });

        text_proof5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });


        getUserDetails(userid,corporate);
        CheckExternalPermissions();
        CheckExternalReadPermissions();
        return view;
    }

    public boolean validatecustomerdetail1(){

        if (et_firstname.getText().toString().length()==0){
            et_firstname.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (et_lastname.getText().toString().length()==0){
            et_lastname.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (et_mobileNumber.getText().toString().length()==0){
            et_mobileNumber.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (et_mobileNumber.getText().toString().length() < 10){
            et_mobileNumber.setError(getString(R.string.mobilevalidation));
            return false;
        }
        else if (et_customerId.getText().toString().length()==0){
            et_customerId.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (et_kitnumber.getText().toString().length()==0){
            et_kitnumber.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (et_kitnumber.getText().toString().length() < 9 ){
            et_kitnumber.setError(getString(R.string.kitvalidation));
            return false;
        }
        else if (et_mailId.getText().toString().length()==0){
            et_mailId.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (txt_dateofbirth.getText().toString().length()==0){
            Toast.makeText(getActivity(), R.string.emptydob_txt, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(titleSpinner.getSelectedItemPosition()==0){
            Toast.makeText(getActivity(), R.string.emptytitle, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(genderSpinner.getSelectedItemPosition()==0){
            Toast.makeText(getActivity(), R.string.emptygender, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(cardtypeSpinner.getSelectedItemPosition()==0){
            Toast.makeText(getActivity(), R.string.emptycardtype, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(corporateSpinner.getSelectedItemPosition()==0){
            Toast.makeText(getActivity(), R.string.emptycorporate, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }



    public boolean validatecustomerdetail2(){
        if(stateSpinner.getSelectedItemPosition()==0){
            Toast.makeText(getActivity(), R.string.stateempty_txt, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (et_addressline.getText().toString().length()==0){
            et_addressline.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if(et_addressline1.getText().toString().length()==0){
            et_addressline1.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if(et_city.getText().toString().length()==0){
            et_city.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if(et_pincode.getText().toString().length()==0){
            et_pincode.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if (et_pincode.getText().toString().length() < 6 ){
            et_pincode.setError(getString(R.string.pincodevalidation));
            return false;
        }

        return true;
    }

    public boolean validatecustomerdetail3(){
        if(proofSpinner.getSelectedItemPosition()==0){
            Toast.makeText(getActivity(), R.string.id_typeempty, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(kyctypeSpinner.getSelectedItemPosition()==0){
            Toast.makeText(getActivity(), R.string.kyc_typeempty, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(et_idnumber.getText().toString().length()==0){
            et_idnumber.setError(getString(R.string.emptyfield_txt));
            return false;
        }
        else if(idexpiry.getText().toString().length()==0){
            Toast.makeText(getActivity(), R.string.emptyexpiry_txt, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
       // System.out.println("ON ACTIVITY RESULT PICURIII USERDETAIL++++"+picUri+resultCode);
        if(resultCode == 0){
            if (proofphoto.equals("proof1")) {
             if(imgclick1)
                 imgclick1 = true;
            }
            else if (proofphoto.equals("proof2")) {
                if(imgclick2)
                    imgclick2 = true;
            }
            else if (proofphoto.equals("proof3")) {
                if(imgclick3)
                    imgclick3 = true;
            }
            else if (proofphoto.equals("proof4")) {
                if(imgclick4)
                    imgclick4 = true;
            }
            else if (proofphoto.equals("proof5")) {
                if(imgclick5)
                    imgclick5 = true;
            }
        }
        switch (requestCode) {
            case REQUEST_CAMERA:
                if ((resultCode == RESULT_OK) && (requestCode == REQUEST_CAMERA)) {
                    //picUri = getPhotoFileUri(photoFileName);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    Bitmap bitmap = BitmapFactory.decodeFile(imageFilePath,options);
                    //System.out.println("imagepath createimagefile++++"+imageFilePath);

                    if (proofphoto.equals("proof1")) {
                        if (Constants.hasImage(imageView2) && !imgclick1) {
                            parts.add(prepareFilePart(proofSpinner1.getSelectedItem().toString(),picUri,REQUEST_CAMERA));
                            parts.remove(parts.size()-2);
                            imageView2.setImageBitmap(bitmap);
                        } else {
                            imgclick1 = false;
                            imageView2.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner1.getSelectedItem().toString(),picUri,REQUEST_CAMERA));
                        }
                    } else if (proofphoto.equals("proof2")) {
                        if (Constants.hasImage(imageView3) && !imgclick2) {
                            imageView3.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner2.getSelectedItem().toString(),picUri1,REQUEST_CAMERA));
                            parts.remove(parts.size()-2);
                        } else {
                            imgclick2 = false;
                            imageView3.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner2.getSelectedItem().toString(),picUri1,REQUEST_CAMERA));
                        }
                    } else if (proofphoto.equals("proof3")) {
                        if (Constants.hasImage(imageView4) && !imgclick3) {
                            imageView4.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner3.getSelectedItem().toString(),picUri2,REQUEST_CAMERA));
                            parts.remove(parts.size()-2);
                        } else {
                            imgclick3 = false;
                            imageView4.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner3.getSelectedItem().toString(),picUri2,REQUEST_CAMERA));
                        }
                    } else if (proofphoto.equals("proof4")) {
                        if (Constants.hasImage(imageView5) && !imgclick4) {
                            imageView5.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner4.getSelectedItem().toString(),picUri3,REQUEST_CAMERA));
                            parts.remove(parts.size()-2);
                        } else {
                            imgclick4 = false;
                            imageView5.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner4.getSelectedItem().toString(),picUri3,REQUEST_CAMERA));
                        }
                    } else if (proofphoto.equals("proof5")) {
                        if (Constants.hasImage(imageView6) && !imgclick5) {
                            imageView6.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner5.getSelectedItem().toString(),picUri4,REQUEST_CAMERA));
                            parts.remove(parts.size()-2);
                        } else {
                            imgclick5 = false;
                            imageView6.setImageBitmap(bitmap);
                            parts.add(prepareFilePart(proofSpinner5.getSelectedItem().toString(),picUri4,REQUEST_CAMERA));
                        }
                    }
                }
        }
    }


    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri,int from) {
        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
        File file;
        if (from == PICK_IMAGE_REQUEST) {
            file = FileUtils.getFile(getActivity(), fileUri);
            //System.out.println("file path"+file.getAbsolutePath());
        } else {
            file = getFilePathFromURI(getActivity(),fileUri);
            //System.out.println("file path"+file.getAbsolutePath());
        }

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(getActivity().getContentResolver().getType(fileUri)),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }


    public  File getFilePathFromURI(Context context, Uri contentUri) {
        //copy file and send new file path
        String fileName = getFileName(contentUri);
        if (!TextUtils.isEmpty(fileName)) {
            File mediaStorageDir = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);
            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
//                Log.d(APP_TAG, "failed to create directory");
            }
            File copyFile = new File(mediaStorageDir.getPath() + File.separator + fileName);
            copy(context, contentUri, copyFile);
            return copyFile;
        }
        return null;
    }

    public static String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IoUtils.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void getUserDetails(final String id,final String corporate){
        Constants.showLoading(getActivity());
        final ArrayList<Spinner> spinners = new ArrayList<>();
        spinners.add(proofSpinner1);
        spinners.add(proofSpinner2);
        spinners.add(proofSpinner3);
        spinners.add(proofSpinner4);
        spinners.add(proofSpinner5);

        final ArrayList<ImageView> imageViews = new ArrayList<>();
        imageViews.add(imageView2);
        imageViews.add(imageView3);
        imageViews.add(imageView4);
        imageViews.add(imageView5);
        imageViews.add(imageView6);

        final ArrayList<LinearLayout> signupprooflays = new ArrayList<>();
        signupprooflays.add(signupprooflay1);
        signupprooflays.add(signupprooflay2);
        signupprooflays.add(signupprooflay3);
        signupprooflays.add(signupprooflay4);
        signupprooflays.add(signupprooflay5);

        final ArrayList<LinearLayout> prooflinearlays = new ArrayList<>();
        prooflinearlays.add(prooflinearlay1);
        prooflinearlays.add(prooflinearlay2);
        prooflinearlays.add(prooflinearlay3);
        prooflinearlays.add(prooflinearlay4);
        prooflinearlays.add(prooflinearlay5);

        Constants.getEkycServicesAPI(getActivity(),corporate).getUserDetails(id, new Callback<UserDetailsResponseTo>() {
            @Override
            public void success(UserDetailsResponseTo userDetailsResponseTo, Response response) {
                  Constants.dismissloading();
                if (!Constants.checkIfException(getActivity(),userDetailsResponseTo)) {
                    et_firstname.setText(userDetailsResponseTo.getResult().getRegistrationData().getFirstName());
                    et_lastname.setText(userDetailsResponseTo.getResult().getRegistrationData().getLastName());
                    String mobile = userDetailsResponseTo.getResult().getRegistrationData().getContactNo().replace("+91","");
                    et_mobileNumber.setText(mobile);
                    et_customerId.setText(userDetailsResponseTo.getResult().getRegistrationData().getEntityId());
                    et_kitnumber.setText(userDetailsResponseTo.getResult().getRegistrationData().getKitNo());
                    et_mailId.setText(userDetailsResponseTo.getResult().getRegistrationData().getEmailAddress());
                    txt_dateofbirth.setText(userDetailsResponseTo.getResult().getRegistrationData().getSpecialDate());
                    if(userDetailsResponseTo.getResult().getRegistrationData().getTitle().equals("Mr"))
                        titleSpinner.setSelection(1);
                    else
                        titleSpinner.setSelection(2);
                    if(userDetailsResponseTo.getResult().getRegistrationData().getGender().equals("M"))
                        genderSpinner.setSelection(1);
                    else
                        genderSpinner.setSelection(2);
                    if(userDetailsResponseTo.getResult().getRegistrationData().getCardType().equals("P"))
                        cardtypeSpinner.setSelection(1);
                    else
                        cardtypeSpinner.setSelection(2);
                    corporateSpinner.setSelection(1);
                    stateSpinner.setSelection(getIndex(stateSpinner,userDetailsResponseTo.getResult().getRegistrationData().getState()));
                    et_addressline.setText(userDetailsResponseTo.getResult().getRegistrationData().getAddress());
                    et_addressline1.setText(userDetailsResponseTo.getResult().getRegistrationData().getAddress2());
                    et_city.setText(userDetailsResponseTo.getResult().getRegistrationData().getCity());
                    et_pincode.setText(userDetailsResponseTo.getResult().getRegistrationData().getPincode());
                    proofSpinner.setSelection(getIndex(proofSpinner,userDetailsResponseTo.getResult().getRegistrationData().getIdType()));
                    kyctypeSpinner.setSelection(getIndex(kyctypeSpinner,userDetailsResponseTo.getResult().getRegistrationData().getKycStatus()));
                    et_idnumber.setText(userDetailsResponseTo.getResult().getRegistrationData().getIdNumber());
                    if(userDetailsResponseTo.getResult().getRegistrationData().getIdExpiry() != null)
                        idexpiry.setText(userDetailsResponseTo.getResult().getRegistrationData().getIdExpiry());

                    if(userDetailsResponseTo.getResult().getReason() != null)
                    {
                        reasoncard.setVisibility(View.VISIBLE);
                        reasondetailtxt.setText(userDetailsResponseTo.getResult().getReason());
                    }
                    else{
                        reasoncard.setVisibility(View.GONE);
                    }

                    if (userDetailsResponseTo.getResult().getKycData()!=null) {
                        for (int i=0; i < userDetailsResponseTo.getResult().getKycData().getDocuments().size();i++){
                            if(i>0)
                             signupprooflays.get(i-1).setVisibility(View.GONE);

                            prooflinearlays.get(i).setVisibility(View.VISIBLE);
                            spinners.get(i).setSelection(getIndex(spinners.get(i),userDetailsResponseTo.getResult().getKycData().getDocuments().get(i).getDocumentType()));
                            setImageBitmapBase64(userDetailsResponseTo.getResult().getKycData().getDocuments().get(i).getBase64String(),imageViews.get(i));
                        }
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                  Constants.dismissloading();
            }
        });
    }

    public void setImageBitmapBase64(String base64,ImageView imageview){
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length,options);
        imageview.setImageBitmap(decodedByte);
    }

    private int getIndex(Spinner spinner, String myString){
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                return i;
            }else if (spinner.getItemAtPosition(i).toString().equals(myString)){
                return i;
            }
        }
        return 0;
    }

    private void getCameraPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            }
        }
    }

    private void CheckExternalPermissions() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
            }
        }
    }
    @TargetApi(Build.VERSION_CODES.M)
    private void CheckExternalReadPermissions() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_READ);
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_READ);
            }
        }
    }
    private boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }

    private File createImageFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        imageFileName = photoFileName + timeStamp + "_";
        File storageDir =
                getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();

        return image;
    }

    public void getUploadpicOption() {
        CheckExternalPermissions();
        CheckExternalReadPermissions();
        getCameraPermission();
       /* StrictMode.VmPolicy.Builder strictbuilder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(strictbuilder.build());
*/
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            Intent pictureIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);

            if (pictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                //Create a file to store the image
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File
                }
                if (photoFile != null) {
                    if (proofphoto.equals("proof1")) {
                        picUri = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri);
                    } else if (proofphoto.equals("proof2")) {
                        picUri1 = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri1);
                    } else if (proofphoto.equals("proof3")) {
                        picUri2 = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri2);
                    } else if (proofphoto.equals("proof4")) {
                        picUri3 = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri3);
                    } else if (proofphoto.equals("proof5")) {
                        picUri4 = FileProvider.getUriForFile(getActivity(), "com.anto.kycapp.provider", photoFile);
                        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                picUri4);
                    }
                    startActivityForResult(pictureIntent,
                            REQUEST_CAMERA);
                }
            }

        } else {
            // Toast.makeText(getActivity(), "You dont have permission to access Camera!", Toast.LENGTH_LONG).show();
            getCameraPermission();
        }
    }


    public void showUploadeddialog(final Activity activity){
        final Dialog uploadeddialog = new Dialog(activity,android.R.style.Theme_Material_Dialog_Alert);
        uploadeddialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        uploadeddialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        uploadeddialog.setCancelable(false);
        uploadeddialog.setContentView(R.layout.signupalert_view);

        TextView yes = uploadeddialog.findViewById(R.id.textYes);
        TextView signup = uploadeddialog.findViewById(R.id.textSignmeUp);
        signup.setText(R.string.updateTxt);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerreg4.setVisibility(View.GONE);
                customerreg1.setVisibility(View.VISIBLE);
                reasoncard.setVisibility(View.VISIBLE);
                usercheckonce = true;
                uploadeddialog.dismiss();
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadeddialog.dismiss();
                updateUser();
            }
        });

        uploadeddialog.show();
    }

    public void updateUser(){
        Constants.showLoading(getActivity());

        final String json = new Gson().toJson(adduserrequestTO);
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        //loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                String token = "Basic "+Constants.serviceUrls.SESSIONTOKEN;
                //System.out.println("requestInterceptor main"+token);

                Request original = chain.request();

                Request.Builder builder = original.newBuilder();
                builder.header("Content-Type","multipart/form-data");
                builder.header("Authorization",token);


                Request request = builder.method(original.method(), original.body())
                        .build();

                Log.e("header in main",request.header("Authorization"));
                Log.e("header in main",request.header("Content-Type"));
                return chain.proceed(request);
            }
        });
        httpClient.addInterceptor(loggingInterceptor);


        Retrofit client = new Retrofit.Builder()
                .baseUrl(BuildConfig.KycBaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        RequestBody jsonstring = createPartFromString(json);
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("jsonBody",jsonstring);
        //System.out.println("Content type++++"+jsonstring.contentType()+jsonstring.toString());

        GetUsersServiceAPI getserviceApi = client.create(GetUsersServiceAPI.class);
        getserviceApi.updateUser(map,parts).enqueue(new retrofit2.Callback<AgentLoginResponseTO>() {
            @Override
            public void onResponse(Call<AgentLoginResponseTO> call, retrofit2.Response<AgentLoginResponseTO> response) {
                Constants.dismissloading();
                //Toast.makeText(MainActivity.this, "Successfully registered!", Toast.LENGTH_SHORT).show();
                if (response.isSuccessful()) {
                    Toast.makeText(getActivity(), "Successfully registered!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(),MainActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    try {
                        Gson gson = new Gson();
                        ResponseTO responseTO = gson.fromJson(response.errorBody().charStream(), ResponseTO.class);
                        Toast.makeText(getActivity(), responseTO.getException().getDetailMessage(), Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Proxy server error", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<AgentLoginResponseTO> call, Throwable t) {
                Constants.dismissloading();
                System.out.println("Erorrrr"+t.getMessage());
                Toast.makeText(getActivity(), "Error in upload", Toast.LENGTH_SHORT).show();
            }
        });

    }



}
