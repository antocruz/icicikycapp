package com.anto.kycapp;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.anto.kycapp.Models.EkycResponseTO;
import com.anto.kycapp.Utils.FontChangeCrawler;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;


public class AdduserActivity extends AppCompatActivity {
    public int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 3;
    public int MY_PERMISSIONS_REQUEST_INTERNET = 4;
    public int MY_PERMISSIONS_REQUEST_FINGERPRINT = 5;
    public int MULTIPLE_PERMISSIONS = 6;
    String[] permissions = new String[]{
            Manifest.permission.USE_FINGERPRINT,
            Manifest.permission.INTERNET};
    Button gotoscan,gotootp;
    ImageView scanimg,otpimg;
    public Dialog dialog;
    TextView logout_txt,viewLog;
    Process logcat;
    String kitno;
    final StringBuilder log = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addekyc_main);
        FontChangeCrawler fontChanger = new FontChangeCrawler(this.getAssets(), this.getResources().getString(R.string.appfont));
        fontChanger.replaceFonts((ViewGroup)findViewById(R.id.main));

        gotootp = (Button)findViewById(R.id.gotootp);
        gotoscan = (Button)findViewById(R.id.gotoscan);

        scanimg = (ImageView)findViewById(R.id.scanimg);
        otpimg = (ImageView)findViewById(R.id.otpimg);


        kitno= getIntent().getStringExtra("Kitno");



       /* gotoscan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.isNetworkAvailable(AdduserActivity.this)) {
                    Intent intent = new Intent(AdduserActivity.this,AuthenticationActivity.class);
                    intent.putExtra("connectedDevice","Morpho");
                    intent.putExtra("securityToken", Constants.securityToken);
                    intent.putExtra("environment","P"); // Possible values are “P” for production
                    startActivityForResult(intent,1);
                } else {
                    Toast.makeText(AdduserActivity.this, "Check your network", Toast.LENGTH_SHORT).show();
                }
            }
        });


        gotootp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.isNetworkAvailable(AdduserActivity.this)) {
                    Intent intent = new Intent(AdduserActivity.this, OtpAuthentication.class);
                    intent.putExtra("securityToken", Constants.securityToken);
                    intent.putExtra("environment","P"); // Possible values are “P” for production
                    startActivityForResult(intent, 2);
                }
                else{
                    Toast.makeText(AdduserActivity.this, "Check your network", Toast.LENGTH_SHORT).show();
                }
            }
        });*/

        /*scanimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.isNetworkAvailable(AdduserActivity.this)) {
                    Intent intent = new Intent(AdduserActivity.this,AuthenticationActivity.class);
                    intent.putExtra("connectedDevice","Morpho");
                    intent.putExtra("securityToken", Constants.securityToken);
                    intent.putExtra("environment","P"); // Possible values are “P” for production
                    startActivityForResult(intent,1);
                } else {
                    Toast.makeText(AdduserActivity.this, "Check your network", Toast.LENGTH_SHORT).show();
                }
            }
        });*/

       /* otpimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.isNetworkAvailable(AdduserActivity.this)) {
                    Intent intent = new Intent(AdduserActivity.this, OtpAuthentication.class);
                    intent.putExtra("securityToken", Constants.securityToken);
                    intent.putExtra("environment","P"); // Possible values are “P” for production
                    startActivityForResult(intent, 2);
                }
                else{
                    Toast.makeText(AdduserActivity.this, "Check your network", Toast.LENGTH_SHORT).show();
                }
            }
        });*/

        checkPermissions();
         //showAddUserDialog("hjh");
    }
    private boolean checkPermissions() {
        int result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)  {
            List<String> listPermissionsNeeded = new ArrayList<>();
            for (String p : permissions) {
                result = checkSelfPermission(p);
                if (result != PackageManager.PERMISSION_GRANTED) {
                    listPermissionsNeeded.add(p);
                }
            }
            if (!listPermissionsNeeded.isEmpty()) {
                requestPermissions( permissions, MULTIPLE_PERMISSIONS);
                return false;
            }
            return true;
        }
        return true;
    }

    @Override
    protected  void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == 1){
            //Toast.makeText(this, "The resultCode is"+resultCode+"Result Failure"+RESULT_CANCELED, Toast.LENGTH_SHORT).show();
            if(resultCode == RESULT_OK){
                String response = data.getStringExtra("data");
              //  System.out.println("Responsefrom"+response);
                try {
                    if (response!=null) {
                        EkycResponseTO responseTo = new Gson().fromJson(response, EkycResponseTO.class);
                        // Toast.makeText(this, responseTo.getTransactionId(), Toast.LENGTH_SHORT).show();

                        if(responseTo.getStatus().equals("Y"))
                            showAddUserDialog(responseTo.getTransactionId());
                        else
                            Toast.makeText(this, "eKyc verification failed", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Response null", Toast.LENGTH_SHORT).show();
                    }

                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

        }
        else if(requestCode == 2){
            if(resultCode == RESULT_OK){
                String response = data.getStringExtra("data");
               // System.out.println("Responsefrom"+response);
                try {
                    if (response!=null) {
                        EkycResponseTO responseTo = new Gson().fromJson(response, EkycResponseTO.class);
                        // Toast.makeText(this, responseTo.getTransactionId(), Toast.LENGTH_SHORT).show();

                        if(responseTo.getStatus().equals("Y"))
                            showAddUserDialog(responseTo.getTransactionId());
                        else
                            Toast.makeText(this, "eKyc verification failed", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Response null", Toast.LENGTH_SHORT).show();
                    }
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }

            }
        }
    }




    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if(requestCode == MULTIPLE_PERMISSIONS){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permissions granted.

            } else {
                String perStr = "";
                for (String per : permissions) {
                    perStr += "\n" + per;
                }
                // permissions list of don't granted permission
            }
            return;

        }
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! do the
                // calendar task you need to do.
            } else {
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            }
            return;

            // other 'switch' lines to check for other
            // permissions this app might request
        }
        if (requestCode == MY_PERMISSIONS_REQUEST_INTERNET) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! do the
                // calendar task you need to do.
            } else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            }
            return;

            // other 'switch' lines to check for other
            // permissions this app might request
        }

        if (requestCode == MY_PERMISSIONS_REQUEST_FINGERPRINT) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! do the
                // calendar task you need to do.
            } else {
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            }
            return;
            // other 'switch' lines to check for other
            // permissions this app might request
        }
    }

    public void showAddUserDialog(final String ekyno){
        dialog = new Dialog(this,android.R.style.Theme_Material_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.mobilenumber_view);
        FontChangeCrawler fontChanger = new FontChangeCrawler(this.getAssets(), this.getResources().getString(R.string.appfont));
        fontChanger.replaceFonts((ViewGroup)dialog.findViewById(R.id.kitnolay));

        final TextView et_kitno;
        final TextView submituser_txt;
        final MKLoader mkLoader;


        et_kitno= (TextView)dialog.findViewById(R.id.et_kit);

        if(kitno!=null && !kitno.isEmpty()){
            et_kitno.setText(kitno);
            et_kitno.setEnabled(false);
        }

        submituser_txt= (TextView) dialog.findViewById(R.id.submituser_txt);
        mkLoader = (MKLoader)dialog.findViewById(R.id.mkloader);


        submituser_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                /*if (validatecustomerdetail1(et_kitno)) {
                    submituser_txt.setVisibility(View.GONE);
                    mkLoader.setVisibility(View.VISIBLE);
                    FetchByKitInputTO addkycAgentInputTO = new FetchByKitInputTO();
                    addkycAgentInputTO.setProxyNumber(et_kitno.getText().toString().trim());
                    addkycAgentInputTO.setEkycRefNo(ekyno);

                    Constants.getEkycServicesAPI(AdduserActivity.this,"ekyc").addekyc(addkycAgentInputTO, new Callback<AddkycAgentResponseTO>() {
                        @Override
                        public void success(AddkycAgentResponseTO agentLoginResponseTO, Response response) {

                           dialog.dismiss();
                            if(!Constants.checkIfException(AdduserActivity.this,agentLoginResponseTO)){
                                Toast.makeText(AdduserActivity.this, "User added successfully", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {

                            submituser_txt.setVisibility(View.VISIBLE);
                            mkLoader.setVisibility(View.INVISIBLE);
                            Constants.ShowErrorHandlerMessage(AdduserActivity.this,error);

                        }
                    });

                }*/

            }
        });
        dialog.show();
    }


    public boolean validatecustomerdetail1(EditText et_kitnumber){
      if (et_kitnumber.getText().toString().length()==0){
            //et_kitnumber.setError(getString(R.string.emptyfield_txt));
            Toast.makeText(this, getString(R.string.emptyfield_txt), Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (et_kitnumber.getText().toString().length() < 9 ){
            //et_kitnumber.setError(getString(R.string.kitvalidation));
            Toast.makeText(this, getString(R.string.kitvalidation), Toast.LENGTH_SHORT).show();
            return false;
        }
return true;
    }
}
